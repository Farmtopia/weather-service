const utils = require("./utils");

class InteroperabilityTranslator {
  constructor() {
    this.schemas = {
      jsonldContext: {
        "@context": {
          "@base": "http://localhost:5000/farm_calendar/",
          "fsm": "https://gitlab.com/Farmtopia/farmtopia-semantic-model/-/raw/main/FarmtopiaSemanticModel.rdf#",
          "pcsm": "https://gitlab.com/Farmtopia/farmtopia-semantic-model/-/raw/main/ontologies/ploutos.rdf#",
          "sosa": "http://www.w3.org/ns/sosa/",
          "ssn-ext": "http://www.w3.org/ns/ssn/ext/",
          "saref4agri": "https://saref.etsi.org/saref4agri/",
          "geosparql": "http://www.opengis.net/ont/geosparql/",
          "qudt": "http://qudt.org/schema/qudt/",
          "unit": "http://qudt.org/2.1/vocab/unit",
          "foodie": "http://foodie-cloud.com/model/foodie#",
          "skos": "http://www.w3.org/2004/02/skos/core#",
          "schema": "http://schema.org/",
          "cf": "http://purl.oclc.org/NET/ssnx/cf/cf-property#",
          "ids": "https://w3id.org/idsa/core#",
          "dct": "http://purl.org/dc/terms/",
          "odlr": "http://www.w3.org/ns/odrl/2/",
          "obo": "http://purl.obolibrary.org/obo/",
          "contains": {
            "@id": "fsm:contains",
            "@type": "@id",
            "@container": "@set"
          },
          "nutrientConcentration": {
            "@id": "pcsm:hasNutrientConcentration",
            "@type": "@id",
            "@container": "@set"
          },
          "operatedOn": {
            "@id": "pcsm:isOperatedOn",
            "@type": "@id"
          },
          "geometry": {
            "@id": "geosparql:hasGeometry",
            "@type": "@id"
          },
          "centroid": {
            "@id": "pcsm:centroid",
            "@type": "@id"
          },
          "resourceQuantityUsed": {
            "@id": "fsm:resourceQuantityUsed",
            "@type": "@id"
          },
          "relatedConcept": {
            "@id": "fsm:relatedConcept",
            "@type": "@id"
          },
          "fertilizer": {
            "@id": "pcsm:usesFertilizer",
            "@type": "@id"
          },
          "pesticide": {
            "@id": "pcsm:usesPesticide",
            "@type": "@id"
          },
          "name": "saref4agri:hasName",
          "cropVariety": "pcsm:hasCropVariety",
          "geoJson": {
            "@id": "geosparql:asGeoJSON",
            "@type": "@json"
          },
          "hasArea": {
            "@id": "fsm:hasArea",
            "@type": "@id"
          },
          "unitOfMeasure": {
            "@id": "fsm:unitOfMeasure",
            "@type": "@id"
          },
          "usedProcedure": {
            "@id": "sosa:usedProcedure",
            "@type": "@id"
          },
          "implementedBy": {
            "@id": "sosa:implementedBy",
            "@type": "@id"
          },
          "operationNotes": "fsm:operationNotes",
          "applicationMethod": "pcsm:hasApplicationMethod",
          "startDatetime": "pcsm:hasStartDatetime",
          "endDatetime": "pcsm:hasEndDatetime",
          "activeSubstance": "pcsm:hasActiveSubstance",
          "targetedTowards": "pcsm:isTargetedTowards",
          "numericValue": "qudt:numericValue",
          "vocabCode": "skos:notation",
          "isEcoFarm": "fsm:isEcoFarm",
          "contents": {
            "@id": "sosa:hasMember",
            "@type": "@id",
            "@container": "@set"
          },
          "property": {
            "@id": "sosa:property",
            "@type": "@id"
          },
          "layer": {
            "@id": "fsm:layer",
            "@type": "@id"
          },
          "sourceSensor": {
            "@id": "sosa:madeBySensor",
            "@type": "@id"
          },
          "location": {
            "@id": "fsm:location",
            "@type": "@id"
          },
          "connectors": {
            "@id": "ids:listedConnector",
            "@type": "@id",
            "@container": "@set"
          },
          "connectorCatalog": {
            "@id": "ids:connectorCatalog",
            "@type": "@id",
            "@container": "@set"
          },
          "representation": {
            "@id": "ids:maintainer",
            "@type": "@id"
          },
          "serviceResources": {
            "@id": "dcat:servesDataset",
            "@type": "@id",
            "@container": "@set"
          },
          "serviceType": {
            "@id": "dct:type",
            "@type": "@id",
            "@container": "@set"
          },
          "instance": {
            "@id": "ids:instance",
            "@type": "@id"
          },
          "sliceContents": {
            "@id": "fsm:sliceContents",
            "@type": "@id",
            "@container": "@set"
          },
          "datasetSlices": {
            "@id": "fsm:datasetSlices",
            "@type": "@id",
            "@container": "@set"
          },
          "creator": {
            "@id": "dct:creator",
            "@type": "@id"
          },
          "publisher": {
            "@id": "dct:publisher",
            "@type": "@id"
          },
          "curator": {
            "@id": "dct:curator",
            "@type": "@id"
          },
          "legalName": "schema:legalName",
          "address": "schema:address",
          "email": "schema:email",
          "representationStandard": "ids:representationStandard",
          "license": {
            "@id": "dct:license",
            "@type": "@id"
          },
          "rights": {
            "@id": "dct:rights",
            "@type": "@id"
          },
          "policy": {
            "@id": "odlr:hasPolicy",
            "@type": "@id"
          },
          "permission": {
            "@id": "odlr:permission",
            "@type": "@id"
          },
          "language": "dct:language",
          "title": "dct:title",
          "description": "dct:description",
          "comment": "fsm:comment",
          "label": "fsm:label",
          "toponym": "fsm:toponym",
          "dateTime": "sosa:resultTime",
          "from": "fsm:from",
          "to": "fsm:to",
          "value": "sosa:hasSimpleResult",
          "timestamp": "fsm:timestamp",
          "phenomenonTime": { "@id": "sosa:phenomenonTime" },
          "Atmosphere": { "@id": "cf:atmosphere" },
          "Vegetation": { "@id": "cf:vegetation" },
          "Water": { "@id": "cf:water" },
          "Surface": { "@id": "cf:surface" },
          "Biosphere": { "@id": "fsm:biosphere" },
          "NDVI": { "@id": "fsm:normalized_difference_vegetation_index" },
          "EVI": { "@id": "fsm:enhanced_vegetation_index" },
          "STR": { "@id": "fsm:shortwave_infrared_transform_reflectance" },
          "LST": { "@id": "fsm:land_surface_temperature" },
          "VHP": { "@id": "fsm:vertical_horizontal_polarization" },
          "NDWI": { "@id": "fsm:normalized_difference_water_index" },
          "AirTemperature": { "@id": "fsm:air_temperature" },
          "Celsius": { "@id": "unit:DEG_C" },
          "RelativeHumidity": { "@id": "fsm:relative_humidity" },
          "Percent": { "@id": "unit:PERCENT" },
          "Index": { "@id": "fsm:INDEX" },
          "WindSpeed": { "@id": "fsm:wind_speed" },
          "MeterPerSecond": { "@id": "unit:M-PER-SEC" },
          "KilometerPerHour": { "@id": "unit:KiloM-PER-HR" },
          "PestInfestationRisk": { "@id": "fsm:pest_infestation_risk" },
          "WindDirection": { "@id": "fsm:wind_direction" },
          "Degree": { "@id": "unit:DEG" },
          "Precipitation": { "@id": "fsm:presipitation" },
          "Evapotranspiration": { "@id": "fsm:evapotranspiration" },
          "Millimetre": { "@id": "unit:MilliM" },
          "AirPressure": { "@id": "fsm:air_pressure" },
          "LeafWetness": { "@id": "fsm:leaf_wetness" },
          "LeafTemperature": { "@id": "fsm:leaf_temperature" },
          "Hectopascal": { "@id": "unit:HectoPA" },
          "Millibar": { "@id": "unit:MilliBAR" },
          "Soil": { "@id": "cf:soil_layer" },
          "SolarIrradiance": { "@id": "fsm:solar_irradiance" },
          "SoilTemperature": { "@id": "fsm:soil_temperature" },
          "SoilMoisture": { "@id": "fsm:soil_moisture" },
          "SoilSalinity": { "@id": "fsm:soil_salinity" },
          "UltravioletIrradiance": { "@id": "fsm:ultraviolet_irradiance" },
          "WattPerMetre2": { "@id": "unit:MicroW-PER-M2" },
          "Volt": { "@id": "unit:V" },
          "Hectare": { "@id": "unit:HA" },
          "landType": { "@id": "fsm:landType" },
          "nationalIdentifier": "fsm:nationalIdentifier",
          "agrovocCode": "fsm:agrovocCode",
          "eppoCode": "fsm:eppoCode",
          "OBIClass": {
            "@id": "fsm:OBIClass",
            "@type": "@id"
          },
          "hasIrrigationSystem": {
            "@id": "fsm:hasIrrigationSystem",
            "@type": "@id"
          },
          "plantGrowthStage": "fsm:plantGrowthStage",
          "hasIrrigationType": "pcsm:hasIrrigationType",
          "plantsPerHa": "fsm:plantsPerHa",
          "hasPlantDate": "pcsm:hasPlantDate",
          "hasHarvestDate": "pcsm:hasHarvestDate",
          "aggregation": "fsm:aggregation",
          "metric": "fsm:metric",
          "ratio": "productRatio",

          // pest risk
          "PUCCGR_infestation_risk": { "@id": "fsm:PUCCGR_infestation_risk" },
          "PUCCGT_infestation_risk": { "@id": "fsm:PUCCGT_infestation_risk" },
          "RHIZSO_infestation_risk": { "@id": "fsm:RHIZSO_infestation_risk" },
          "GIBBZE_infestation_risk": { "@id": "fsm:GIBBZE_infestation_risk" },
          "1PHOMG_infestation_risk": { "@id": "fsm:1PHOMG_infestation_risk" },
          "DASGPA_infestation_risk": { "@id": "fsm:DASGPA_infestation_risk" },
          "ARMV00_infestation_risk": { "@id": "fsm:ARMV00_infestation_risk" },
          "PRATNE_infestation_risk": { "@id": "fsm:PRATNE_infestation_risk" },
          "HETDCR_infestation_risk": { "@id": "fsm:HETDCR_infestation_risk" },
          "HETDPA_infestation_risk": { "@id": "fsm:HETDPA_infestation_risk" },
          "PARABU_infestation_risk": { "@id": "fsm:PARABU_infestation_risk" },
          "1MELG_infestation_risk": { "@id": "fsm:1MELG_infestation_risk" },
          "PSILRO_infestation_risk": { "@id": "fsm:PSILRO_infestation_risk" },
          "CARPPO_infestation_risk": { "@id": "fsm:CARPPO_infestation_risk" },
          "PUCCST_infestation_risk": { "@id": "fsm:PUCCST_infestation_risk" },
          "HAPDMA_infestation_risk": { "@id": "fsm:HAPDMA_infestation_risk" },
          "ALTETP_infestation_risk": { "@id": "fsm:ALTETP_infestation_risk" },
          "MELGHA_infestation_risk": { "@id": "fsm:MELGHA_infestation_risk" },
          "TRV000_infestation_risk": { "@id": "fsm:TRV000_infestation_risk" },
          "HETDCA_infestation_risk": { "@id": "fsm:HETDCA_infestation_risk" },
          "LEPTNO_infestation_risk": { "@id": "fsm:LEPTNO_infestation_risk" },
          "HYLERA_infestation_risk": { "@id": "fsm:HYLERA_infestation_risk" },
          "TRIHPR_infestation_risk": { "@id": "fsm:TRIHPR_infestation_risk" },
          "BARABR_infestation_risk": { "@id": "fsm:BARABR_infestation_risk" },
          "1PHYTG_infestation_risk": { "@id": "fsm:1PHYTG_infestation_risk" },
          "SITDMO_infestation_risk": { "@id": "fsm:SITDMO_infestation_risk" },
          "PYRNTE_infestation_risk": { "@id": "fsm:PYRNTE_infestation_risk" },
          "MELIAE_infestation_risk": { "@id": "fsm:MELIAE_infestation_risk" },
          "SCLESC_infestation_risk": { "@id": "fsm:SCLESC_infestation_risk" },
          "MELGIN_infestation_risk": { "@id": "fsm:MELGIN_infestation_risk" },
          "PLASVI_infestation_risk": { "@id": "fsm:PLASVI_infestation_risk" },
          "PUCCHD_infestation_risk": { "@id": "fsm:PUCCHD_infestation_risk" },
          "TRIHPA_infestation_risk": { "@id": "fsm:TRIHPA_infestation_risk" },
          "TRIHTE_infestation_risk": { "@id": "fsm:TRIHTE_infestation_risk" },
          "PUCCTR_infestation_risk": { "@id": "fsm:PUCCTR_infestation_risk" },
          "LEPTMA_infestation_risk": { "@id": "fsm:LEPTMA_infestation_risk" },
          "1PYTHG_infestation_risk": { "@id": "fsm:1PYTHG_infestation_risk" },
          "1COLLG_infestation_risk": { "@id": "fsm:1COLLG_infestation_risk" },
          "1XIPHG_infestation_risk": { "@id": "fsm:1XIPHG_infestation_risk" },
          "HETDTR_infestation_risk": { "@id": "fsm:HETDTR_infestation_risk" },
          "FUSASO_infestation_risk": { "@id": "fsm:FUSASO_infestation_risk" },
          "SEPTTR_infestation_risk": { "@id": "fsm:SEPTTR_infestation_risk" },
          "FUSAOX_infestation_risk": { "@id": "fsm:FUSAOX_infestation_risk" },
          "RHOPPA_infestation_risk": { "@id": "fsm:RHOPPA_infestation_risk" },
          "PRATCR_infestation_risk": { "@id": "fsm:PRATCR_infestation_risk" },
          "1SCLEG_infestation_risk": { "@id": "fsm:1SCLEG_infestation_risk" },
          "3WEEDT_infestation_risk": { "@id": "fsm:3WEEDT_infestation_risk" },
          "PUCCRE_infestation_risk": { "@id": "fsm:PUCCRE_infestation_risk" },
          "1LONGG_infestation_risk": { "@id": "fsm:1LONGG_infestation_risk" },
          "PRATPE_infestation_risk": { "@id": "fsm:PRATPE_infestation_risk" },
          "TRIHSI_infestation_risk": { "@id": "fsm:TRIHSI_infestation_risk" },
          "HETDMA_infestation_risk": { "@id": "fsm:HETDMA_infestation_risk" },
          "MELGAR_infestation_risk": { "@id": "fsm:MELGAR_infestation_risk" },
          "MELGNA_infestation_risk": { "@id": "fsm:MELGNA_infestation_risk" },
          "PYRNTR_infestation_risk": { "@id": "fsm:PYRNTR_infestation_risk" },
          "DEROAG_infestation_risk": { "@id": "fsm:DEROAG_infestation_risk" },
          "ERYSGR_infestation_risk": { "@id": "fsm:ERYSGR_infestation_risk" },
          "MELGFA_infestation_risk": { "@id": "fsm:MELGFA_infestation_risk" },
          "PHYTIN_infestation_risk": { "@id": "fsm:PHYTIN_infestation_risk" },
          "1VERTG_infestation_risk": { "@id": "fsm:1VERTG_infestation_risk" },
          "ERYSGT_infestation_risk": { "@id": "fsm:ERYSGT_infestation_risk" },
          "LAMTEQ_infestation_risk": { "@id": "fsm:LAMTEQ_infestation_risk" },
          "PRATBR_infestation_risk": { "@id": "fsm:PRATBR_infestation_risk" },
          "PHOPSP_infestation_risk": { "@id": "fsm:PHOPSP_infestation_risk" },
          "PUCCSI_infestation_risk": { "@id": "fsm:PUCCSI_infestation_risk" },
          "HETDSC_infestation_risk": { "@id": "fsm:HETDSC_infestation_risk" },
          "PSDMTM_infestation_risk": { "@id": "fsm:PSDMTM_infestation_risk" },
          "XANTKM_infestation_risk": { "@id": "fsm:XANTKM_infestation_risk" },
          "PUCCRT_infestation_risk": { "@id": "fsm:PUCCRT_infestation_risk" },
          "ALTESO_infestation_risk": { "@id": "fsm:ALTESO_infestation_risk" },
          "HETDRO_infestation_risk": { "@id": "fsm:HETDRO_infestation_risk" },
          "DITYDI_infestation_risk": { "@id": "fsm:DITYDI_infestation_risk" },
        },
      },
      itemCollectionsCorrelation: { // TODO add these to data_specifications objects on defaults.json
        // layers (for better visibility)
        "surface": {
          "property": "Surface"
        },
        "atmosphere": {
          "property": "Atmosphere"
        },
        "biosphere": {
          "property": "Biosphere"
        },

        // atmosphere propeeties
        "ambient_temperature": {
          "property": "AirTemperature",
          "unit": "Celsius"
        },
        "ambient_humidity": {
          "property": "RelativeHumidity",
          "unit": "Percent"
        },
        "wind_speed": {
          "property": "WindSpeed",
          "unit": "MeterPerSecond"
        },
        "pressure": {
          "property": "AirPressure",
          "unit": "Hectopascal"
        },
        "wind_direction": {
          "property": "WindDirection",
          "unit": "Degree"
        },
        "precipitation": {
          "property": "Precipitation",
          "unit": "Millimetre"
        },

        // sentinel-hub properties
        "NDVI": {
          "property": "NDVI",
          "unit": "NDVI"      // TODO unit field is not used in the schema
        },
        "EVI": {
          "property": "EVI",
          "unit": "EVI"
        },
        "NDWI_WB": {
          "property": "NDWI-WB",
          "unit": "NDWI-WB"
        },
        "NDWI_WC": {
          "property": "NDWI-WC",
          "unit": "NDWI-WC"
        },
        "STR": {
          "property": "STR",
          "unit": "STR"
        },
        "VH_POLARIZATION": {
          "property": "VHP",
          "unit": "VHP"
        },
        "LST": {
          "property": "LST",
          "unit": "LST"
        },

        // pest EPPO codes
        "PUCCGR": {
          "property": "PUCCGR_infestation_risk",
          "unit": "Percent"
        },
        "PUCCGT": {
          "property": "PUCCGT_infestation_risk",
          "unit": "Percent"
        },
        "RHIZSO": {
          "property": "RHIZSO_infestation_risk",
          "unit": "Percent"
        },
        "GIBBZE": {
          "property": "GIBBZE_infestation_risk",
          "unit": "Percent"
        },
        "1PHOMG": {
          "property": "1PHOMG_infestation_risk",
          "unit": "Percent"
        },
        "DASGPA": {
          "property": "DASGPA_infestation_risk",
          "unit": "Percent"
        },
        "ARMV00": {
          "property": "ARMV00_infestation_risk",
          "unit": "Percent"
        },
        "PRATNE": {
          "property": "PRATNE_infestation_risk",
          "unit": "Percent"
        },
        "HETDCR": {
          "property": "HETDCR_infestation_risk",
          "unit": "Percent"
        },
        "HETDPA": {
          "property": "HETDPA_infestation_risk",
          "unit": "Percent"
        },
        "PARABU": {
          "property": "PARABU_infestation_risk",
          "unit": "Percent"
        },
        "1MELG": {
          "property": "1MELG_infestation_risk",
          "unit": "Percent"
        },
        "PSILRO": {
          "property": "PSILRO_infestation_risk",
          "unit": "Percent"
        },
        "CARPPO": {
          "property": "CARPPO_infestation_risk",
          "unit": "Percent"
        },
        "PUCCST": {
          "property": "PUCCST_infestation_risk",
          "unit": "Percent"
        },
        "HAPDMA": {
          "property": "HAPDMA_infestation_risk",
          "unit": "Percent"
        },
        "ALTETP": {
          "property": "ALTETP_infestation_risk",
          "unit": "Percent"
        },
        "MELGHA": {
          "property": "MELGHA_infestation_risk",
          "unit": "Percent"
        },
        "TRV000": {
          "property": "TRV000_infestation_risk",
          "unit": "Percent"
        },
        "HETDCA": {
          "property": "HETDCA_infestation_risk",
          "unit": "Percent"
        },
        "LEPTNO": {
          "property": "LEPTNO_infestation_risk",
          "unit": "Percent"
        },
        "HYLERA": {
          "property": "HYLERA_infestation_risk",
          "unit": "Percent"
        },
        "TRIHPR": {
          "property": "TRIHPR_infestation_risk",
          "unit": "Percent"
        },
        "BARABR": {
          "property": "BARABR_infestation_risk",
          "unit": "Percent"
        },
        "1PHYTG": {
          "property": "1PHYTG_infestation_risk",
          "unit": "Percent"
        },
        "SITDMO": {
          "property": "SITDMO_infestation_risk",
          "unit": "Percent"
        },
        "PYRNTE": {
          "property": "PYRNTE_infestation_risk",
          "unit": "Percent"
        },
        "MELIAE": {
          "property": "MELIAE_infestation_risk",
          "unit": "Percent"
        },
        "SCLESC": {
          "property": "SCLESC_infestation_risk",
          "unit": "Percent"
        },
        "MELGIN": {
          "property": "MELGIN_infestation_risk",
          "unit": "Percent"
        },
        "PLASVI": {
          "property": "PLASVI_infestation_risk",
          "unit": "Percent"
        },
        "PUCCHD": {
          "property": "PUCCHD_infestation_risk",
          "unit": "Percent"
        },
        "TRIHPA": {
          "property": "TRIHPA_infestation_risk",
          "unit": "Percent"
        },
        "TRIHTE": {
          "property": "TRIHTE_infestation_risk",
          "unit": "Percent"
        },
        "PUCCTR": {
          "property": "PUCCTR_infestation_risk",
          "unit": "Percent"
        },
        "LEPTMA": {
          "property": "LEPTMA_infestation_risk",
          "unit": "Percent"
        },
        "1PYTHG": {
          "property": "1PYTHG_infestation_risk",
          "unit": "Percent"
        },
        "1COLLG": {
          "property": "1COLLG_infestation_risk",
          "unit": "Percent"
        },
        "1XIPHG": {
          "property": "1XIPHG_infestation_risk",
          "unit": "Percent"
        },
        "HETDTR": {
          "property": "HETDTR_infestation_risk",
          "unit": "Percent"
        },
        "FUSASO": {
          "property": "FUSASO_infestation_risk",
          "unit": "Percent"
        },
        "SEPTTR": {
          "property": "SEPTTR_infestation_risk",
          "unit": "Percent"
        },
        "FUSAOX": {
          "property": "FUSAOX_infestation_risk",
          "unit": "Percent"
        },
        "RHOPPA": {
          "property": "RHOPPA_infestation_risk",
          "unit": "Percent"
        },
        "PRATCR": {
          "property": "PRATCR_infestation_risk",
          "unit": "Percent"
        },
        "1SCLEG": {
          "property": "1SCLEG_infestation_risk",
          "unit": "Percent"
        },
        "3WEEDT": {
          "property": "3WEEDT_infestation_risk",
          "unit": "Percent"
        },
        "PUCCRE": {
          "property": "PUCCRE_infestation_risk",
          "unit": "Percent"
        },
        "1LONGG": {
          "property": "1LONGG_infestation_risk",
          "unit": "Percent"
        },
        "PRATPE": {
          "property": "PRATPE_infestation_risk",
          "unit": "Percent"
        },
        "TRIHSI": {
          "property": "TRIHSI_infestation_risk",
          "unit": "Percent"
        },
        "HETDMA": {
          "property": "HETDMA_infestation_risk",
          "unit": "Percent"
        },
        "MELGAR": {
          "property": "MELGAR_infestation_risk",
          "unit": "Percent"
        },
        "MELGNA": {
          "property": "MELGNA_infestation_risk",
          "unit": "Percent"
        },
        "PYRNTR": {
          "property": "PYRNTR_infestation_risk",
          "unit": "Percent"
        },
        "DEROAG": {
          "property": "DEROAG_infestation_risk",
          "unit": "Percent"
        },
        "ERYSGR": {
          "property": "ERYSGR_infestation_risk",
          "unit": "Percent"
        },
        "MELGFA": {
          "property": "MELGFA_infestation_risk",
          "unit": "Percent"
        },
        "PHYTIN": {
          "property": "PHYTIN_infestation_risk",
          "unit": "Percent"
        },
        "1VERTG": {
          "property": "1VERTG_infestation_risk",
          "unit": "Percent"
        },
        "ERYSGT": {
          "property": "ERYSGT_infestation_risk",
          "unit": "Percent"
        },
        "LAMTEQ": {
          "property": "LAMTEQ_infestation_risk",
          "unit": "Percent"
        },
        "PRATBR": {
          "property": "PRATBR_infestation_risk",
          "unit": "Percent"
        },
        "PHOPSP": {
          "property": "PHOPSP_infestation_risk",
          "unit": "Percent"
        },
        "PUCCSI": {
          "property": "PUCCSI_infestation_risk",
          "unit": "Percent"
        },
        "HETDSC": {
          "property": "HETDSC_infestation_risk",
          "unit": "Percent"
        },
        "PSDMTM": {
          "property": "PSDMTM_infestation_risk",
          "unit": "Percent"
        },
        "XANTKM": {
          "property": "XANTKM_infestation_risk",
          "unit": "Percent"
        },
        "PUCCRT": {
          "property": "PUCCRT_infestation_risk",
          "unit": "Percent"
        },
        "ALTESO": {
          "property": "ALTESO_infestation_risk",
          "unit": "Percent"
        },
        "HETDRO": {
          "property": "HETDRO_infestation_risk",
          "unit": "Percent"
        },
        "DITYDI": {
          "property": "DITYDI_infestation_risk",
          "unit": "Percent"
        }
      }
    };
  }

  generateLocationId = () => Math.floor(100 + Math.random() * 900);

  parseGeometry = (spatialEntity) => JSON.parse(spatialEntity).geometry;

  createGeometry = (locationId, geometry) => ({
    "geometry": {
      "@id": "geometry" + locationId,
      "@type": "geometry",
      "geoJson": geometry
    }
  });

  createLocation = (layer, locationId, geometry) => {
    const self = this;
    return {
      "@id": locationId,
      "@type": layer === "Surface" ? "fsm:RegionOfInterest" : "fsm:PointOfInterest",
      "geometry": self.createGeometry(locationId, geometry),
    }
  }

  createLocationSlice = (layer, spatialEntity, locationId) => {
    const self = this;
    const geometry = self.parseGeometry(spatialEntity);
    return {
      "@id": "locationsSlice",
      "@type": "fsm:Slice",
      "sliceContents": [
        self.createLocation(layer, locationId, geometry)
      ]
    }
  }

  createSerializedObjectSurface = (layer, operation, propertyKey, propertyUnit, dataPoint, counter) => {
    const [min, max, mean, stDev, sampleCount, noDataCount, from, to] = Object.values(dataPoint);
    let phenomenonTime = {
      "@id": `statisticsPeriod_${++counter.value}`,
      "from": {
        "@id": from.replace('Z', '+03:00'),
        "timestamp": from.replace('Z', '+03:00'),
      },
      "to": {
        "@id": to.replace('Z', '+03:00'),
        "timestamp": to.replace('Z', '+03:00')
      },
    };
    return {
      "@id": `${layer}_${operation}_Collection_${counter.value}`,
      "@type": "fsm:DataPointsGroup",
      "layer": layer,
      "phenomenonTime": phenomenonTime,
      "sliceContents": [
        {
          "@id": `${propertyKey}_${operation}_Collection_${counter.value}`,
          "@type": "fsm:DataPointsGroup",
          "property": propertyKey,
          "unitOfMeasure": propertyUnit, //"Celsius",
          "contents": [
            {
              "@id": `${propertyKey}_${operation}_Min_${counter.value}`,
              "@type": "fsm:Observation",
              "aggregation": "statistical",
              "metric": "min",
              "value": min
            },
            {
              "@id": `${propertyKey}_${operation}_Max_${counter.value}`,
              "@type": "fsm:Observation",
              "aggregation": "statistical",
              "metric": "max",
              "value": max
            },
            {
              "@id": `${propertyKey}_${operation}_Mean_${counter.value}`,
              "@type": "fsm:Observation",
              "aggregation": "statistical",
              "metric": "mean",
              "value": mean
            }
          ]
        }
      ]
    };
  }

  createcontentsAtmosphere = (propertyDataPoints, propertyKey, operation, counter) => {
    let contents = []
    for (const index in propertyDataPoints) {
      const dataPoint = propertyDataPoints[index];
      const [timestamp, value] = Object.values(dataPoint);
      const item = {
        "@id": `${propertyKey}_${operation}_${++counter.value}`, //"TemperatureAirObservation1",
        "@type": operation === "Observation" ? "fsm:Observation": "fsm:Prediction",
        "dateTime": utils.timestampToDate(timestamp),
        "value": value
      };
      contents.push(item);
    }
    return contents;
  }

  createLayerObject = (layer, operation, counter) => ({
    "@id": `${layer}_${operation}Collection_${++counter.value}`,
    "@type": "fsm:DataPointsGroup",
    "layer": layer,
    "sliceContents": []
  });

  createSerializedObjectAtmosphere = (propertyKey, propertyUnit, operation, counter, contents) => ({
    "@id": `${propertyKey}_${operation}_Collection_${++counter.value}`,
    "@type": "fsm:DataPointsGroup",
    "property": propertyKey,
    "unitOfMeasure": propertyUnit,
    "contents": contents
  })

  createcontentsBiosphere = (propertyDataPoints, propertyKey, operation, counter) => {
    let contents = []
    for (const index in propertyDataPoints) {
      const dataPoint = propertyDataPoints[index];
      const [timestamp, value] = Object.values(dataPoint);
      const item = {
        "@id": `${propertyKey}_${operation}_${++counter.value}`, //"TemperatureAirObservation1",
        "@type": "fsm:Prediction",
        "dateTime": utils.timestampToDate(timestamp / 1000),
        "value": value
      };
      contents.push(item);
    }
    return contents;
  }

  createMeasurementsSlice = (layer, operation, locationId, serializedObjects) => ({
    "@id": "measurementsSlice",
    "@type": "fsm:Slice",
    "sliceContents": [
      {
        "@id": `${layer}_${operation}s`,
        "@type": "fsm:DataPointsGroup",
        "location": {
          "@id": locationId,
        },
        "sliceContents": serializedObjects
      }
    ]
  });

  findOntologiesSource = (contextContent) => {
    // function to extract ontology dependencies from other ontologies
    const self = this;
    let sources = []
    for (const item of contextContent) {
      let source = structuredClone(self.schemas.jsonldContext["@context"][item]);
      if ( utils.isObject(source) ) {
        source = source["@id"];
      }
      source = source.split(":")[0];
      sources.push(source);
    }
    return [... new Set(contextContent.concat(sources))];
  }

  createContext = (contextContent, semanticData) => {
    // function to create custom context object, including only fields relevant to graph
    const self = this;
    let context = structuredClone(self.schemas.jsonldContext["@context"]);
    contextContent = contextContent.concat(utils.allKeys(semanticData))
    contextContent = contextContent.filter(e=>!e.includes("@"));
    contextContent = [... new Set(contextContent)].filter(e => e in context);
    contextContent =  self.findOntologiesSource(contextContent);

    return Object.fromEntries(Object.entries(context).filter(([key]) => contextContent.includes(key)));
  };

  serializeToJsonLd(data) {
    const self = this;
    const correlation = self.schemas.itemCollectionsCorrelation;
    let semanticData = {    // response object
        "@id": null, // "NP_gaiasense_serviceResource_representation_instance_1",
        "@type": "fsm:Dataset",
        "datasetSlices": [
        ]
    };

    const layer = correlation[data.layer].property;
    const operation = data.operation; // observation or prediction
    let contextContent = [];  // array of context keys relevant to response
    const spatialEntities = Object.keys(data.data);

    semanticData["@id"] = `FarmtopiaIntegrated${operation}Data`;

    for (const spatialEntity of spatialEntities) {
      // create locationSlice
      const locationId = self.generateLocationId().toString();
      semanticData.datasetSlices[0] = self.createLocationSlice(layer, spatialEntity, locationId);

      let counterLayer = {value: 0};
      let serializedObjects = [];   // placeholder array
      let measurementsSlice = {};   // placeholder object
      let layerObject = self.createLayerObject(layer, operation, counterLayer);

      const properties = Object.keys(data.data[spatialEntity]);
      for (const property of properties) {
        const propertyDataPoints = data.data[spatialEntity][property];
        const propertyKey = correlation[property].property; // AirTemperature
        const propertyUnit = correlation[property].unit;    // Celsius
        contextContent.push(propertyKey);
        contextContent.push(propertyUnit);
        let contents = [];               // placeholder array
        let counterItem = {value: 0};
        let counterObject = {value: 0};
        // create measurementsSlice
        switch (layer) {
          case "Surface":
            for (const index in propertyDataPoints) {
              const dataPoint = propertyDataPoints[index]
              let object = self.createSerializedObjectSurface(layer, operation, propertyKey, propertyUnit, dataPoint, counterObject);
              serializedObjects.push(object);
            }
            layerObject.sliceContents.push(serializedObjects);
            break;
          case "Atmosphere":
            contents = self.createcontentsAtmosphere(propertyDataPoints, propertyKey, operation, counterItem);
            serializedObjects = self.createSerializedObjectAtmosphere(propertyKey, propertyUnit, operation, counterObject, contents);
            layerObject.sliceContents.push(serializedObjects);
            break;
          case "Biosphere":
            contents = self.createcontentsBiosphere(propertyDataPoints, propertyKey, operation, counterItem);
            serializedObjects = self.createSerializedObjectAtmosphere(propertyKey, propertyUnit, operation, counterObject, contents);
            layerObject.sliceContents.push(serializedObjects);
            break;
        }
      }
      measurementsSlice = self.createMeasurementsSlice(layer, operation, locationId, layerObject);
      semanticData.datasetSlices[1] = measurementsSlice;
    }
    let responseObj = {};
    responseObj["@graph"] = semanticData;
    responseObj["@graph"]["@id"] = Math.random().toString(36).substring(7);
    // create context
    let context = self.createContext(contextContent, responseObj);
    // context["@base"] = `http://localhost:5000/${data.layer}`;
    responseObj = Object.assign({ "@context": context }, responseObj);
    return responseObj;
  }
};

module.exports = InteroperabilityTranslator;

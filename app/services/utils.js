const turf = require('@turf/turf');

const sentinelHubEvalscripts = {
  ndvi:
    `//VERSION=3
function setup() {
  return {
    input: [{
      bands: [ "B04", "B08", "SCL", "dataMask" ]
    }],
    output: [
      {
        id: "NDVI",
        bands: 1
      },
      {
        id: "dataMask",
        bands: 1
      }]
  }
}
function evaluatePixel(sample) {
  let ndvi = (sample.B08 - sample.B04) / (sample.B08 + sample.B04)
  var validNDVIMask = 1;
  if (sample.B08 + sample.B04 == 0 ) {
      validNDVIMask = 0;
  }
  if ((isNaN(sample.B08)) || (isNaN(sample.B04))) {
    validNDVIMask = 0;
  }
  var noWaterMask = 1
  // only exclude water and clouds
  if (sample.SCL != 4 ){
      noWaterMask = 0;
  }
  return {
      NDVI: [ndvi],
      dataMask: [sample.dataMask * validNDVIMask * noWaterMask]
  }
}`,
  evi:
    `//VERSION=3
function setup() {
  return {
    input: [{
      bands: [ "B02", "B04", "B08", "SCL", "dataMask" ]
    }],
    output: [
      {
        id: "EVI",
        bands: 1
      },
      {
        id: "dataMask",
        bands: 1
      }]
  }
}
function evaluatePixel(sample) {
  let evi = 2.5 * (sample.B08 - sample.B04) / ((sample.B08 + 6.0 * sample.B04 - 7.5 * sample.B02) + 1.0);
  var validMask = 1
  if (sample.B08 + sample.B04 + sample.B02 == 0 ){
      validMask = 0
  }
  if ((evi > 1.0) || (evi < -1.00)) {
    validMask = 0;
  }
  var noWaterMask = 1
  if (sample.SCL == 6 ){
      noWaterMask = 0
  }
  return {
      EVI: [evi],
      dataMask: [sample.dataMask * validMask * noWaterMask]
  }
}`,
  ndwi_wb:
    `//VERSION=3
function setup() {
  return {
    input: [{
      bands: ["B03", "B08", "SCL", "dataMask"]
    }],
    output: [
      {
        id: "NDWI_WB",
        bands: 1
      },
      {
        id: "dataMask",
        bands: 1
      }]
  }
}
function computeIndex(sample) {
  return (sample.B03 - sample.B08) / (sample.B03 + sample.B08)
}
function computeMask(sample) {
  let dataMask = sample.dataMask;
  let otherMask = 1;
  if ((sample.SCL == 9)||(sample.SCL==8)||(sample.SCL==7)) {
    otherMask = 0;
  }
  if ((sample.SCL != 6)){
    otherMask = 0;
  }
  return dataMask * otherMask;
}
function evaluatePixel(sample) {
  let ndwi = computeIndex(sample);
  let mask = computeMask(sample);
  return { NDWI_WB: [ndwi], dataMask: [mask] }
}`,
  ndwi_wc:
    `//VERSION=3
function setup() {
  return {
    input: [{
      bands:["B08", "B11", "SCL", "dataMask"],
    }],
    output: [
      {
        id: "NDWI_WC",
        bands: 1
      },
      {
        id: "dataMask",
        bands: 1
      }]
  }
}
function computeIndex(sample) {
  return (sample.B08 - sample.B11) / (sample.B08 + sample.B11)
}
function computeMask(sample) {
  let dataMask = sample.dataMask;
  let otherMask = 1;
  if ((sample.SCL == 9)||(sample.SCL==8)||(sample.SCL==7)) {
    otherMask = 0;
  }
  return dataMask * otherMask;
}
function evaluatePixel(sample) {
  let ndwi = computeIndex(sample);
  let mask = computeMask(sample);
  return { NDWI_WC: [ndwi], dataMask: [mask] }
}`,
  str:
    `//VERSION=3
function setup() {
  return {
    input: [{
      bands:["B12",  "dataMask"],
    }],
    output: [
      {
        id: "STR",
        bands: 1
      },
      {
        id: "dataMask",
        bands: 1
      }]
  }
}
function computeMask(sample) {
  let dataMask = sample.dataMask;
  let validMask = 1;
  if ((sample.B12 == 0) || (sample.B12 == Number.POSITIVE_INFINITY) || (isNaN(sample.B12))){
    validMask = 0;
  }
  return dataMask * validMask;
}
function computeIndex(sample) {
  return ((1 - sample.B12)**2) / (2 * sample.B12);
}
function evaluatePixel(sample) {
  let str = computeIndex(sample);
  let mask = computeMask(sample);
  return {
      STR: [str],
      dataMask: [mask]
  }
}`,
  lst:
    `//VERSION=3 (auto-converted from 1)
var band = "B11";
var option = 0;
var minC = 0;
var maxC = 50;
var NDVIs = 0.2;
var NDVIv = 0.8;
var waterE = 0.991;
var soilE = 0.966;
var vegetationE = 0.973;
var C = 0.009;
var bCent = (band == "B10") ? 0.000010895 : 0.000012005;
var rho = 0.01438; // m K
if (option == 2) {
  minC = 0;
  maxC = 20;
}
function setup() {
  return {
    input: [{
      bands: [ "B03", "B04", "B05", "B10", "B11", "dataMask", ]
    }],
    mosaicking: "ORBIT",
    output: [
      {
        id: "LST",
        bands: 1
      },
      {
        id: "dataMask",
        bands: 1
      }]
  }
}
function computeMask(samples) {
  let mask = samples.dataMask;
  let otherMask = 1;
  if ((isNaN(samples.B03)) || (isNaN(samples.B04)) || (isNaN(samples.B05)) || (isNaN(samples.B10)) || (isNaN(samples.B11))) {
    otherMask = 0;
  }
  return mask * otherMask;
}
function LSEcalc(NDVI, Pv) {
  var LSE;
  if (NDVI < 0) {
    LSE = waterE;
  } else if (NDVI < NDVIs) {
    LSE = soilE;
  } else if (NDVI > NDVIv) {
    LSE = vegetationE;
  } else {
    LSE = vegetationE * Pv + soilE * (1 - Pv) + C;
  }
  return LSE;
}
function evaluatePixel(samples) {
  var LSTmax = -999;
  var LSTavg = 0;
  var LSTstd = 0;
  var reduceNavg = 0;
  var N = samples.length;
  var LSTarray = [];
  for (var i = 0; i < N; i++) {
    var Bi = (band == "B10") ? samples[i].B10 : samples[i].B11;
    var B03i = samples[i].B03;
    var B04i = samples[i].B04;
    var B05i = samples[i].B05;
    if ((Bi > 173 && Bi < 65000) && (B03i > 0 && B04i > 0 && B05i > 0)) {
      var b10BTi = Bi - 273.15;
      var NDVIi = (B05i - B04i) / (B05i + B04i);
      var PVi = Math.pow(((NDVIi - NDVIs) / (NDVIv - NDVIs)), 2);
      var LSEi = LSEcalc(NDVIi, PVi);
      var LSTi = (b10BTi / (1 + (((bCent * b10BTi) / rho) * Math.log(LSEi))));
      LSTavg = LSTavg + LSTi;
      if (LSTi > LSTmax) { LSTmax = LSTi; }
      LSTarray.push(LSTi);
    } else {
      ++reduceNavg;
    }
  }
  N = N - reduceNavg;
  LSTavg = LSTavg / N;
  for (var j = 0; j < LSTarray.length; j++) {
    LSTstd = LSTstd + (Math.pow(LSTarray[j] - LSTavg, 2));
  }
  LSTstd = (Math.pow(LSTstd / (LSTarray.length - 1), 0.5));
  let outLST = (option == 0) ? LSTavg : (option == 1) ? LSTmax : LSTstd;
  return {
      LST: [outLST],
      dataMask: [1]
  }
}`,
  vh_polarization:
    `
//VERSION=3
function setup() {
  return {
    input: [{ bands: ["VH", "dataMask" ] }],
    output: [
      {
        id: "VH",
        bands: 1,
	sampleType: "AUTO"
      },
      {
        id: "dataMask",
        bands: 1
      }]
  }
}
function evaluatePixel(sample) {
  return {
      VH: [Math.max(0, Math.log(sample.VH) * 0.21714724095 + 1)],
      dataMask: [sample.dataMask]
  }
}`,
}

const transformationFunctions = {
  // generic transformation functions

  transformSpatial: function (stringWKT) {
    // util function to transorm WKT string to GeoJSON
    let type;
    let coordinates = [];

    if (stringWKT.includes("POINT")) {
      type = "Point";
      coordinates = stringWKT;
      coordinates = coordinates.replace("POINT (", "");
      coordinates = coordinates.replace(")", "");
      coordinates = coordinates.split(" ");
      coordinates = coordinates.map(parseFloat);
      coordinates = coordinates.reverse();
    }
    if (stringWKT.includes("POLYGON")) {
      type = "Polygon";
      let string = stringWKT;
      string = string.replace("POLYGON((", "");
      string = string.split(",");
      string.forEach((substring) => {
        substring = substring.replace("(", "");
        substring = substring.replace("))", "");
        substring = substring.split(", ")
        let coordPair = substring[0].split(" ").map(parseFloat);
        coordPair = coordPair.reverse();
        coordinates.push(coordPair);
      })
      coordinates = [coordinates];
    }
    // return geojson
    return {
      "type": "Feature",
      "geometry": {
        "type": type,
        "coordinates": coordinates,
      }
    };
  },

  timestampToDate: function (timestamp) {
    // util function to transform unix timestamp string of format:
    // 2024-03-01T00:00:00Z
    // case : timestamp is UNIX timestamp
    if (timestamp % 1 === 0) {
      let date = new Date(timestamp * 1000);
      let fullYear = date.getFullYear().toString().padStart(2, '0');
      let month = (date.getMonth() + 1).toString().padStart(2, '0');
      let day = date.getDate().toString().padStart(2, '0');
      let hours = date.getHours().toString().padStart(2, '0');
      let minutes = date.getMinutes().toString().padStart(2, '0');
      let seconds = date.getSeconds().toString().padStart(2, '0');
      let fullDate = `${fullYear}-${month}-${day}T${hours}:${minutes}:${seconds}Z`;
      return fullDate;
    } else {
      // case: timestamp is already in date format
      return timestamp;
    }
  },

  timeDifferenceFromDates: function (from, to) {
    // util function to find the time difference in days between two timestamps or Dates
    let fromDate, toDate;
    // case: from and to are timestamps
    if ((from % 1 == 0) && (to % 1 == 0)) {
      fromDate = new Date(transformationFunctions.timestampToDate(from));
      toDate = new Date(transformationFunctions.timestampToDate(to));
    } else {
      // case: from and to are dates
      fromDate = new Date(from);
      toDate = new Date(to);
    }
    const diff = Math.abs(toDate.getTime() - fromDate.getTime());
    // const diffDays = diff / ( 1000 * 60 * 60 * 24 );
    const diffDays = Math.ceil(diff / (1000 * 3600 * 24));
    return diffDays;
  },

  // sentinel-hub transformation functions

  hexToFloat: function (colorHex) {
    // util function to transform hex color value to float numerical value for sentinelhub OGC requests
    let chunks = [];
    let tmp, i;
    if (colorHex.length === 3) {
      tmp = colorHex.split("");
      for (i = 0; i < 3; i++) {
        chunks.push(parseInt(tmp[i] + "" + tmp[i], 16));
      }
    } else if (colorHex.length === 6) {
      tmp = colorHex.match(/.{2}/g);
      for (i = 0; i < 3; i++) {
        chunks.push(parseInt(tmp[i], 16));
      }
    } else {
      throw new Error('"+colorHex+" is not a valid hex format');
    }
    return 2 * (chunks[0] / 255.0) - 1;
  },

  setWidth: function (geometry) {
    // util function to dynamically set imgWidth according to geometry
    // S2L2A collection (sentinel2) has 1500 meters/pixel upper limit
    const hres = 1400; // maximum meters / pixel is 1500. set here to 1400 to avoid calc overhead
    let polygon = turf.polygon(geometry.geometry.coordinates);
    let bbox = turf.bbox(polygon);
    let hline = turf.lineString([[bbox[0], bbox[1]], [bbox[2], bbox[1]]]);
    let hlineLength = turf.length(hline, { units: "kilometers" }) * 1000;
    let width = hlineLength / hres;
    return Math.ceil(width);
  },

  setHeight: function (geometry) {
    // util function to dynamically set imgHeight according to geometry
    // S2L2A collection (sentinel2) has 1500 meters/pixel upper limit
    const vres = 1400; // maximum meters / pixel is 1500. set here to 1400 to avoid calc overhead
    let polygon = turf.polygon(geometry.geometry.coordinates);
    let bbox = turf.bbox(polygon);
    let vline = turf.lineString([[bbox[0], bbox[1]], [bbox[0], bbox[3]]]);
    let vlineLength = turf.length(vline, { units: "kilometers" }) * 1000;
    let height = vlineLength / vres;
    return Math.ceil(height);
  },

  transformSpatialForStatistics: function (spatial) {
    // util function to create geojson object from WKT string and also reverse latitude/longitude
    // sentinel-hub statistics requires longitude-latitude
    // sentinel-hub ogc requires latitude-longitude
    return transformationFunctions.transformSpatial(spatial, false);
  },

  loadFromForStatistics: function (from) {
    // util function to load/transform "from" for sentinel-hub statistics requests
    // case: "from" undefined -> means the statistics request is a dependent request of a current operation. from = yesterday
    // case: "from" defined -> return date
    if (typeof from == "undefined") {
      let now = Math.floor(new Date().getTime() / 1000);
      let yesterday = now - 86400;
      yesterday = transformationFunctions.timestampToDate(yesterday);
      return transformationFunctions.timestampToDate(yesterday);
    } else {
      return transformationFunctions.timestampToDate(from);
    }
  },

  loadToForStatistics: function (to) {
    // util function to load/transform "to" for sentinel-hub statistics requests
    // case: "to" undefined -> means the statistics request is a dependent request of a current operation. to = now
    // case: "to" defined -> return date
    if (typeof to == "undefined") {
      let now = Math.floor(new Date().getTime() / 1000);
      return transformationFunctions.timestampToDate(now);
    } else {
      return transformationFunctions.timestampToDate(to);
    }
  },

  loadAggregationInterval: function (aggregationInterval, from, to) {
    // util function to load aggregation interval for sentinel-hub statistics requests
    // cases:
    // 1. "from" and "to" undefined - current operation -> to = now, from = yesterday
    // 2. requested interval is bigger than the time difference of "from" and "to" -> return time difference
    const self = transformationFunctions;
    const timeDifferenceInDays = Math.floor(self.timeDifferenceFromDates(from, to));
    const requestedInterval = Math.floor(aggregationInterval);
    if (requestedInterval > timeDifferenceInDays) {
      return `P${timeDifferenceInDays}D`;
    } else {
      return `P${requestedInterval}D`;
    }
  },


  loadEvalscript: function (property) {
    // util function to load evalscript for sentinel-hub requests
    let evalscript;
    evalscript = sentinelHubEvalscripts[property];
    return evalscript;
  },

  loadLayer: function (property) {
    // util function to load layer for sentinel-hub requests
    switch (property.toUpperCase()) {
      case "LST":
        return "LANDSAT8";
      case "VH_POLARIZATION":
        return "VH_POLARIZATION";
      default:
        return "TRUE_COLOR";
    }
  },

  loadSatelite: function (property) {
    // util function to load satelite for sentinel-hub requests
    if (property == "vh_polarization") {
      return "sentinel-1-grd";
    } else if (property == "lst") {
      return "landsat-ot-l1";
    } else {
      return "sentinel-2-l2a";
    }
  },

  // ipm-decisions transformation functions
  eppoCodeValidity: function (eppoCode, availableEppoCodes) {
    // util function to validate cropEppoCode and pestEppoCode for ipm-decisions request
    if (availableEppoCodes.includes(eppoCode)) {
      return true;
    } else {
      return false;
    }
  },

  extractDssModelOptions: function (dss, cropEppoCode, pestEppoCode) {
    // util function to find valid model for location and return dssModelOptions object - ipmdecisions
    const models = dss.models;
    for (const index in models) {
      let model = structuredClone(models[index]);
      if ((model.pests.includes(pestEppoCode)) && (model.crops.includes(cropEppoCode))) {
        return JSON.stringify([{
          "dssId": dss.id,
          "dssName": dss.name,
          "dssModelName": model.name,
          "dssModelId": model.id,
          "dssModelVersion": model.version,
          "dssExecutionType": model.execution.type,
          "dssEndPoint": dss.id,
          "cropEppoCode": cropEppoCode,
          "pestEppoCode": pestEppoCode,
          "dssVersion": dss.version,
        }]);
      }
    }
    return undefined;
  },

  buildDssBaseEndpointUri: function (farmId) {
    return `https://platform.ipmdecisions.net/api/upr/farms/${farmId}/dss`;
  },

  buildDssInstanceEndpointUri: function (dssInstanceId) {
    return `https://platform.ipmdecisions.net/api/upr/dss/${dssInstanceId}?days=0`;
  }
}


function extractCoordsFromGeojson(geojson) {
  // util function to extract coordinates listed as tuples from geojson object
  let coordsList = []
  if (geojson.type == "MultiPolygon") {
    geojson.coordinates[0].forEach((d1) => {
      d1.forEach((d2) => {
        coordsList.push(d2);
      })
    })
  }
  if (geojson.type == "Polygon") {
    geojson.coordinates[0].forEach((d1) => {
      coordsList.push(d1);
    })
  }
  if (geojson.type == "Point") {
    coordsList.push(geojson.coordinates);
  }
  return coordsList;
}

function isGeoJson(object) {
  if (typeof object !== "object") {
    return false;
  }
  if (("type" in object) && ("coordinates" in object)) {
    return true;
  } else {
    return false;
  }
}

function isObject(a) {
  // util function that checks whether a variable is an object
  return (!!a) && (a.constructor === Object);
}

function dateToTimestamp(date, format="DD-MM-YYYY") {
  if (!date || typeof date !== 'string') return null;

  const parts = date.includes("/") ? date.split("/") : date.split("-");

  if (format === "DD-MM-YYYY") {
    const dateObj = new Date(parts[2], parts[1] - 1, parts[0]);
    return dateObj.getTime();
  } else if (format === "YYYY-MM-DD") {
    const dateObj = new Date(parts[0], parts[1] - 1, parts[2]);
    return dateObj.getTime();
  } else {
    throw new Exception("Invalid date format");
  }
}

function timestampToDate(timestamp) {
  if (timestamp % 1 === 0) {
    let date = new Date(timestamp * 1000);
    let fullYear = date.getFullYear().toString().padStart(2, '0');
    let month = (date.getMonth() + 1).toString().padStart(2, '0');
    let day = date.getDate().toString().padStart(2, '0');
    let hours = date.getHours().toString().padStart(2, '0');
    let minutes = date.getMinutes().toString().padStart(2, '0');
    let seconds = date.getSeconds().toString().padStart(2, '0');
    let fullDate = `${fullYear}-${month}-${day}T${hours}:${minutes}:${seconds}Z`;
    return fullDate;
  } else {
    return timestamp;
  }
}

isObject = (a) => { return (!!a) && (a.constructor === Object); }

function allKeys(object) {
  // util function to return a list of all keys in object of any depth
  if (!isObject(object)) { return }
  let keys = [];
  for (const [key, value] of Object.entries(object)) {
    if (isObject(value)) {
      keys = keys.concat(allKeys(value));
    }
    if (Array.isArray(value)) {
      value.forEach(item => { keys = keys.concat(allKeys(item)) })
    }
    keys.push(key)
  }
  return [... new Set(keys)].filter(e => !(e === undefined)).filter(e => !e.includes("@"));
}

function convertSpeedUnits(value, format_from="km/h", format_to="m/s") {
  if (value === null | value === undefined | typeof value !== "number") {
    throw new Exception("Invalid speed value: ${value}");
  }

  if (format_from === "km/h") {
    if (format_to === "m/s") {
      return Number(value * 0.27777777777778).toFixed(2);
    }
  }

}

module.exports = {
  transformationFunctions,
  extractCoordsFromGeojson,
  isGeoJson,
  isObject,
  dateToTimestamp,
  timestampToDate,
  isObject,
  allKeys,
  convertSpeedUnits
}

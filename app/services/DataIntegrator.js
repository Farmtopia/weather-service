const { MeasurementModel } = require('../models');
const { PointModel } = require('../models');
const { PredictionModel } = require('../models');

const util = require('util')
const log = (obj) => { console.log(util.inspect(obj, {showHidden: false, depth: null, colors: true})) }

class Measurement {
  constructor(id, value, timestamp, data_specification) {
    this.id = id;
    this.value = value;
    this.timestamp = timestamp;
    this.data_specification = data_specification || null;
  }

  static async generate(params) {
    let { id, value, timestamp, data_specification } = await MeasurementModel.createMeasurement(new MeasurementModel(params));
    return new Measurement(id, value, timestamp, data_specification);
  }

  static async find(query) {
    let measurementModels = await MeasurementModel.readMeasurements(query, {}, 0, 20000);
    return Object.values(measurementModels).map((measurementModel) => {
      const { id, value, timestamp, data_specification } = measurementModel;
      return new Measurement(id, value, timestamp, data_specification);
    })
  }
};
class Point {
  constructor(id, type, title, location, root, parent, childrens) {
    this.id = id;
    this.type = type;
    this.title = title;
    this.location = location || {};
    this.root = root || {};
    this.parent = parent || {};
    this.childrens = childrens || [];
  }
  static async load(id) {
    const { type, title, location, root_point_id, parent_id, children_ids } = await PointModel.readPoint(id);
    return new Point(id, type, title, location, root_point_id, parent_id, children_ids);
  }
  static async generate(params) {
    let pointModel = await PointModel.createPoint(new PointModel(params));
    const { id, type, title, location, root_point_id, parent_id, children_ids } = pointModel;
    return new Point(id, type, title, location, root_point_id, parent_id, children_ids);
  }
  async save(params) {
    const { id } = params;
    const { type, title, location, root_point_id, parent_id, children_ids } = await PointModel.updatePoint(id, params);
    return new Point(id, type, title, location, root_point_id, parent_id, children_ids);
  }
  async remove() {
    await PointModel.deletePoint(this.id);
    return this.id;
  }

  static async find(query) {
    let pointModels = await PointModel.readPoints(query, {}, 0, 20000);
    return Object.values(pointModels).map((PointModel) => {
      let { id, type, title, location, root_point_id, parent_id, children_ids } = PointModel;
      return new Point(id, type, title, location, root_point_id, parent_id, children_ids);
    })
  }
};
class Prediction {
  constructor(id, value, createdAt, timestamp, source, spatialEntity, data_specification, type, timeWindow) {
    this.id = id;
    this.value = value;
    this.createdAt = createdAt;
    this.timestamp = timestamp;
    this.source = source;
    this.spatialEntity = spatialEntity;
    this.data_specification = data_specification || null;
    this.type = type;
    this.timeWindow = timeWindow;
  }

  static async generate(params) {
    let { id, value, createdAt, timestamp, source, spatialEntity, data_specification, type, timeWindow } = await PredictionModel.createPrediction(new PredictionModel(params));
    return new Prediction(id, value, createdAt, timestamp, source, spatialEntity, data_specification, type, timeWindow);
  }
  static async find(query) {
    let predictionModels = await PredictionModel.readPredictions(query, {}, 0, 20000);
    return Object.values(predictionModels).map((predictionModel) => {
      const { id, value, createdAt, timestamp, source, spatialEntity, data_specification, type, timeWindow } = predictionModel;
      return new Prediction(id, value, createdAt, timestamp, source, spatialEntity, data_specification, type, timeWindow);
    })
  }

};
const APIRouter = require('./APIRouter');
const RequestHandler = require('./RequestHandler');
const InteroperabilityTranslator = require('./InteroperabilityTranslator');
const utils = require("./utils");
const bodyParser = require("body-parser");
const axios = require('axios');

class DataIntegrator {
  constructor(app, userManager) {
    this.app = app;
    this.userManager = userManager;
    this.systemOptions = {
      "features": {             // layers as keys, properties as values - !substitute data_specifications!
        "atmosphere":{
          "ambient_temperature": {
            "sources":[
              {
                "provider": "openWeatherMaps",
                "services": [
                  {"openWeatherMapsCurrent": "2.5"},
                  {"openWeatherMapsForecast": "2.5"},
                ],
              }
            ]
          },
          "ambient_humidity": {
            "sources":[
              {
                "provider": "openWeatherMaps",
                "services": [
                  {"openWeatherMapsCurrent": "2.5"},
                  {"openWeatherMapsForecast": "2.5"},
                ],
              }
            ]
          },
          "wind_speed": {
            "sources":[
              {
                "provider": "openWeatherMaps",
                "services": [
                  {"openWeatherMapsCurrent": "2.5"},
                  {"openWeatherMapsForecast": "2.5"},
                ],
              }
            ]
          },
          "wind_direction": {
            "sources":[
              {
                "provider": "openWeatherMaps",
                "services": [
                  {"openWeatherMapsCurrent": "2.5"},
                  {"openWeatherMapsForecast": "2.5"},
                ],
              }
            ]
          },
          "precipitation": {
            "sources":[
              {
                "provider": "openWeatherMaps",
                "services": [
                  {"openWeatherMapsCurrent": "2.5"},
                  {"openWeatherMapsForecast": "2.5"},
                ],
              }
            ]
          },
        },
        "surface": {
          "ndvi": {
            "sources":[
              {
                "provider": "sentinel-hub",
                "services": [
                  {"earthObservationStatistical": "eustat"},
                ],
              },
            ],
          },
          "evi": {
            "sources":[
              {
                "provider": "sentinel-hub",
                "services": [
                  {"earthObservationStatistical": "eustat"},
                ],
              }
            ]
          },
          "ndwi_wb": {
            "sources":[
              {
                "provider": "sentinel-hub",
                "services": [
                  {"earthObservationStatistical": "eustat"},
                ],
              }
            ]
          },
          "ndwi_wc": {
            "sources":[
              {
                "provider": "sentinel-hub",
                "services": [
                  {"earthObservationStatistical": "eustat"},
                ],
              }
            ]
          },
          "str": {
            "sources":[
              {
                "provider": "sentinel-hub",
                "services": [
                  {"earthObservationStatistical": "eustat"},
                ],
              }
            ]
          },
          "vh_polarization": {
            "sources":[
              {
                "provider": "sentinel-hub",
                "services": [
                  {"earthObservationStatistical": "eustat"},
                ],
              }
            ]
          },
          "lst": {
            "sources":[
              {
                "provider": "sentinel-hub",
                "services": [
                  {"earthObservationStatistical": "uswest2stat"},
                ],
              }
            ]
          },
        },
        "biosphere": {
          "pest": {
            "sources": [
              {
                "provider": "IPMDecisions",
                "services": [
                  {"IPMRisk": "upr/dss"}
                ]
              },
            ],
            "parameters": { },
          },
        }
      },

      "operations": {
        "discover": { // what data is available
          "layers": ["biopshpere"],
        },
        "initialize": {
          "layers": ["bioshpere"],
        },
        "request":{
          "forecast": {
            "when": "> now",
            "layers": ["atmosphere", "biosphere"],
            "data_types": ["prediction"],
          },
          "current": {
            "when": "== now",
            "layers": ["atmosphere", "surface"],
            "data_types": ["observation"],
          },
          "historical": {
            "when": "< now",
            "layers": ["atmosphere", "surface"],
            "data_types": ["observation"],
          }
        }
      },

      "apiServices": {
        "/api/surface/sentinelhub": [
          {
            /* properties:
                ndvi
                evi
                ndwi_wb
                ndwi_wc
                str
                lst
                vh_polarization
             */
            "fn": "surface_sentinelhub"
          }
        ],
        "/api/atmosphere/current/openweathermaps": [
          {
            /* properties:
             */
            "fn": "atmosphere_current_openweathermaps",
          }
        ],
        "/api/atmosphere/forecast/openweathermaps": [
          {
            /* properties:
             */
            "fn": "atmosphere_forecast_openweathermaps",
          }
        ],
        "/api/atmosphere/forecast/openmeteo": [
          {
            /* properties:
             */
            "fn": "atmosphere_forecast_openmeteo"
          }
        ],
        "/api/atmosphere/historical/openmeteo": [
          {
            /* properties:
             */
            "fn": "atmosphere_historical_openmeteo",
          }
        ],
        "/api/biosphere/ipm": [
          {
            /* properties:
                pest:
                  'PUCCGR', 'PUCCGT', 'RHIZSO', 'GIBBZE', '1PHOMG',
                  'DASGPA', 'ARMV00', 'PRATNE', 'HETDCR', 'HETDPA',
                  'PARABU', '1MELG',  'PSILRO', 'CARPPO', 'PUCCST',
                  'HAPDMA', 'XPESTS', 'ALTETP', 'MELGHA', 'TRV000',
                  'HETDCA', 'LEPTNO', 'HYLERA', 'TRIHPR', 'BARABR',
                  '1PHYTG', 'SITDMO', 'PYRNTE', 'MELIAE', 'SCLESC',
                  'MELGIN', 'PLASVI', 'PUCCHD', 'TRIHPA', 'TRIHTE',
                  'PUCCTR', 'LEPTMA', '1PYTHG', '1COLLG', '1XIPHG',
                  'HETDTR', 'FUSASO', 'SEPTTR', 'FUSAOX', 'RHOPPA',
                  'PRATCR', '1SCLEG', '3WEEDT', 'PUCCRE', '1LONGG',
                  'PRATPE', 'TRIHSI', 'DITDE',  'HETDMA', 'MELGAR',
                  'MELGNA', 'PYRNTR', 'DEROAG', 'ERYSGR', 'MELGFA',
                  'PHYTIN', '1VERTG', 'ERYSGT', 'LAMTEQ', 'PRATBR',
                  'MELCGH', 'WEEDSP', 'PHOPSP', 'PUCCSI', 'HETDSC',
                  'PSDMTM', 'XANTKM', 'PUCCRT', 'ALTESO', 'HETDRO',
                  'DITYDI'
                crop:
                  'PIBSS', 'RAPSR', 'LACSS', 'PIBSX',  'TRZAS', 'LYPES',
                  'LOLPE', 'GGGGG', 'ASPOF', 'GGGGGG', 'NNNGW', 'HORUS',
                  'TRZAW', 'ALLCE', 'PAVSA', '3WHEC',  'TRZAX', 'ALLSS',
                  'VICFX', 'ILUPG', 'MABSD', 'MEDSA',  'AVESW', 'VITVI',
                  'AVESP', 'HORVS', 'ALLFI', 'ZEAMX',  'DAUCS', 'PARCR',
                  'HORVX', 'HORVW', 'AVESG', 'BEAUP',  'ILUPS', 'SOLME',
                  'AVESA', 'CIEAR', 'LYPXP', 'TTLRI',  'LOLMU', 'LIUUT',
                  'SOLTU', 'APUGR', 'PYUCO', 'BRSNN',  'HELAN', 'BRSRR',
                  'TRFPR', 'APUGV', 'CICIN', 'SECCS',  'PHSVX', 'TAGPA',
                  'SECCW', 'BRSOK', 'LTHSA', 'TTLWI',  'OATSS', 'APUGD',
                  'ORYSA', 'BRSOB', 'VICSA', 'FRAAN',  'BEAVX', 'BRSOX',
                  'LENCU', 'BRSOL', 'VICFM', 'XCROPS', 'GLXMA', 'BRSSS',
                  'GUIAB', 'TRZDS', 'TTLSO', 'CMASA',  'PHCTA', 'CITLA',
                  'NARSS', 'HUMLU', 'ALLPO', 'BEASS',  'TRFRE'
             */
            // "param": "/property=:property&crop=:crop&pest=:pest&spatial=:spatial&from=:from&to=:to&aggregation=:aggregation",
            "fn": "biosphere_ipm",
          }
        ],
      },
    };
  }

  async init() {
    const self = this;
    self.requestHandler = new RequestHandler(self.app, self, self.userManager);
    self.APIRouter = new APIRouter(self.app, self.requestHandler, self.systemOptions.apiServices);
    self.interoperabilityTranslator = new InteroperabilityTranslator();
    self.requestHandler.initHandlers();
    self.APIRouter.initRoutes();
  }


  async authorizeForService(authorizationConfig) {
    // handles authorization by parsing selectedSource.authorization
    if (typeof authorizationConfig == "undefined") { return {}; }

    if ( authorizationConfig.type == "token" ) {
      let token;
      let expiration = authorizationConfig.expiration;
      // case: token is static and stored in the config
      if ( "token" in authorizationConfig ) {
        token = authorizationConfig.token;
      }
      // case: token is obtained through request
      if ( "request" in authorizationConfig ) {
        let authRequestConfig = authorizationConfig.request.config
        const response = await axios.request(authRequestConfig)
        token = response;
      }
      return token
    }
  }

  delay(mseconds) {     // experimental - for IPM
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve("waited 2 seconds");
      }, mseconds);
    })
  }

  async sentinelHubRequest(params) {
    const self = this;

    // select source
    let uri;
    if ( params.property == "lst" ){
      uri = "https://services-uswest2.sentinel-hub.com/api/v1/statistics";
    } else {
      uri = "https://services.sentinel-hub.com/api/v1/statistics";
    }
    const authConfig = {
      "type": "token",
      "request": {
        "config": {
          "method": "post",
          "maxBodyLength": Infinity,
          "url": "https://services.sentinel-hub.com/auth/realms/main/protocol/openid-connect/token",
          "headers": {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          "data": `client_id=${process.env.SENTINEL_HUB_CLIENT_ID}&client_secret=${process.env.SENTINEL_HUB_CLIENT_SECRET}&grant_type=client_credentials`,
        },
      },
      "expiration": "60"
    };

    // correlate params
    const geometry = utils.transformationFunctions.transformSpatial(params.spatial);
    const aggregationInterval = utils.transformationFunctions.loadAggregationInterval(params.interval, params.from, params.to);
    let token =  await self.authorizeForService(authConfig)
    token = "Bearer " + token.data.access_token;

    // build body
    const body = {
      "input": {
        "bounds": {
          "properties": {
            "crs": "http://www.opengis.net/def/crs/OGC/1.3/CRS84",
          },
          "geometry": geometry.geometry
        },
        "data": [
          {
            "type": utils.transformationFunctions.loadSatelite(params.property),
            "dataFilter": {},
          }
        ]
      },
      "aggregation": {
        "timeRange": {
          "from": utils.transformationFunctions.loadFromForStatistics(params.from),
          "to": utils.transformationFunctions.loadToForStatistics(params.to),
        },
        "aggregationInterval": {
          "of": aggregationInterval,
        },
        "width": utils.transformationFunctions.setWidth(geometry),
        "height": utils.transformationFunctions.setHeight(geometry),
        "evalscript": utils.transformationFunctions.loadEvalscript(params.property),
      },
      "calculations": {
        "default": {},
      }
    };

    // build headers
    const headers = {
      "Authorization": token,
      "Content-Type": "application/json",
      "Accept": "application/json",
    };

    const config = {
      method: "post",
      maxBodyLength: Infinity,
      url: uri,
      data: body,
      headers: headers,
    };
    return config
  }

  sentinelHubResponse(response, params) {
    const data = response.data.data;
    const spatialEntity = JSON.stringify(utils.transformationFunctions.transformSpatial(params.spatial));
    const property = params.property.toUpperCase();
    let result = {
      "layer": params.layer,
      "operation": "Observation",
      "data" : {
      }
    };

    for (const index in data) {
      let feature =  data[index];
      let interval = feature.interval;
      let outputs = feature.outputs;
      outputs = outputs[Object.keys(outputs)[0]].bands.B0.stats;
      result.data[spatialEntity] = result.data[spatialEntity] || {};
      result.data[spatialEntity][property] = result.data[spatialEntity][property] || [];
      result.data[spatialEntity][property].push(Object.assign(outputs, interval));
    }
    return result;
  }

  openWeatherCurrentRequest(params) {
    const uri = "http://api.openweathermap.org/data/2.5/weather?";
    const token = process.env.OPEN_WEATHER_MAPS_TOKEN;
    const geometry = utils.transformationFunctions.transformSpatial(params.spatial);
    const config = uri + new URLSearchParams({
      "units": "metric",
      "lat": geometry.geometry.coordinates[1],
      "lon": geometry.geometry.coordinates[0],
      "appid": token,
    }).toString();
    return config;
  }

  openWeatherCurrentResponse(response, params) {
    const spatialEntity = JSON.stringify(utils.transformationFunctions.transformSpatial(params.spatial));
    const data = response.data;
    const timestamp = data.dt;
    return {  "layer": params.layer,
              "operation": "Observation",
              "data" : {
                [spatialEntity] : {
                  "ambient_temperature": [[timestamp, data.main.temp]],
                  "ambient_humidity": [[timestamp, data.main.humidity]],
                  "wind_speed": [[timestamp, data.wind.speed]],
                  "wind_direction": [[timestamp, data.wind.deg]],
                  "precipitation": [[timestamp, data.rain?.["1h"] ?? 0]],
                }
              }
           };
  }

  openWeatherForecastRequest(params){
    const uri = "http://api.openweathermap.org/data/2.5/forecast?";
    const token = process.env.OPEN_WEATHER_MAPS_TOKEN;
    const geometry = utils.transformationFunctions.transformSpatial(params.spatial);
    const config = uri + new URLSearchParams({
      "units": "metric",
      "lat": geometry.geometry.coordinates[1],
      "lon": geometry.geometry.coordinates[0],
      "appid": token,
    }).toString();

    return config;
  }

  openWeatherForecastResponse(response, params){
    const spatialEntity = JSON.stringify(utils.transformationFunctions.transformSpatial(params.spatial));
    const data = response.data.list;

    let result = {
      "layer": params.layer,
      "operation": "Prediction",
      "data" : {
        [spatialEntity]: {
          "ambient_temperature": [],
          "pressure": [],
          "ambient_humidity": [],
          "wind_speed": [],
          "wind_direction": [],
          "precipitation": []
        }
      }
    };

    data.forEach( (dataPoint) => {
      let timestamp = dataPoint.dt;
      let values = {
        "ambient_temperature": [timestamp, dataPoint.main.temp],
        "pressure": [timestamp, dataPoint.main.pressure],
        "ambient_humidity": [timestamp, dataPoint.main.humidity],
        "wind_speed": [timestamp, dataPoint.wind.speed],
        "wind_direction": [timestamp, dataPoint.wind.deg],
        "precipitation": [timestamp, dataPoint.rain?.["1h"] ?? 0],
      };
      Object.keys(values).forEach( key => {
        result.data[spatialEntity][key].push(values[key]);
      });
    });
    return result;
  }

  openMeteoForecastRequest(params) {
    const uri = "https://api.open-meteo.com/v1/forecast?"
    const geometry = utils.transformationFunctions.transformSpatial(params.spatial);
    const latitude = geometry.geometry.coordinates[1];
    const longitude = geometry.geometry.coordinates[0];
    const config = uri + new URLSearchParams({
      "latitude": latitude,
      "longitude": longitude,
      "hourly": [
        "temperature_2m", 
        "surface_pressure",
        "relative_humidity_2m", 
        "wind_speed_10m",
        "wind_direction_10m",
        "precipitation"
      ],
      "timeformat": "unixtime",
      "temporal_resolution": "hourly_3",
      "wind_speed_unit": "ms",
      "past_days": 0,
      "forecast_days": 5,
      "models": ["ecmwf_ifs025"],
      "timezone": "GMT"
    })
    return config;
  }

  openMeteoForecastResponse(response, params) {
    const spatialEntity = JSON.stringify(utils.transformationFunctions.transformSpatial(params.spatial));
    const data = response.data;

    let result = {
      "layer": params.layer,
      "operation": "Prediction",
      "data": {
        [spatialEntity]: {
          "ambient_temperature": [],
          "pressure": [],
          "ambient_humidity": [],
          "wind_speed": [],
          "wind_direction": [],
          "precipitation": []
        }
      }
    };

    const range = data.hourly.time.length;
    let values = [];
    for (let i = 0; i < range; ++i) {
      const timestamp = data.hourly.time[i];
      values = {
        "ambient_temperature": [timestamp, data.hourly["temperature_2m"][i]],
        "pressure": [timestamp, data.hourly["surface_pressure"][i]],
        "ambient_humidity": [timestamp, data.hourly["relative_humidity_2m"][i]],
        //"wind_speed": [timestamp, Number(utils.convertSpeedUnits(data.hourly["wind_speed_10m"][i], "km/h", "m/s"))],
        "wind_speed": [timestamp, data.hourly["wind_speed_10m"][i]],
        "wind_direction": [timestamp, data.hourly["wind_direction_10m"][i]],
        "precipitation": [timestamp, data.hourly["precipitation"][i]]
      };
      Object.keys(values).forEach(key => {
        result.data[spatialEntity][key].push(values[key])
      });
    }
    return result;
  }

  openMeteoHistoricalRequest(params) {
    const uri = "https://historical-forecast-api.open-meteo.com/v1/forecast?"
    const geometry = utils.transformationFunctions.transformSpatial(params.spatial);
    const latitude = geometry.geometry.coordinates[1];
    const longitude = geometry.geometry.coordinates[0];
    const startDate = utils.transformationFunctions.timestampToDate(params.from).split("T")[0]
    const endDate = utils.transformationFunctions.timestampToDate(params.to).split("T")[0];
    const config = uri + new URLSearchParams({
      "latitude": latitude,
      "longitude": longitude,
      "start_date": startDate,
      "end_date": endDate,
      "hourly": [
        "temperature_2m", 
        "surface_pressure",
        "relative_humidity_2m", 
        "wind_speed_10m",
        "wind_direction_10m",
        "precipitation"
      ],
      "timeformat": "unixtime",
      "wind_speed_unit": "ms",
      "timezone": "GMT"
    })
    return config;
  }

  openMeteoHistoricalResponse(response, params) {
    const spatialEntity = JSON.stringify(utils.transformationFunctions.transformSpatial(params.spatial));
    const data = response.data;

    let result = {
      "layer": params.layer,
      "operation": "Observation",
      "data": {
        [spatialEntity]: {
          "ambient_temperature": [],
          "pressure": [],
          "ambient_humidity": [],
          "wind_speed": [],
          "wind_direction": [],
          "precipitation": []
        }
      }
    };

    const range = data.hourly.time.length;
    let values = [];
    for (let i = 0; i < range; ++i) {
      const timestamp = data.hourly.time[i];
      values = {
        "ambient_temperature": [timestamp, data.hourly["temperature_2m"][i]],
        "pressure": [timestamp, data.hourly["surface_pressure"][i]],
        "ambient_humidity": [timestamp, data.hourly["relative_humidity_2m"][i]],
        "wind_speed": [timestamp, data.hourly["wind_speed_10m"][i]],
        "wind_direction": [timestamp, data.hourly["wind_direction_10m"][i]],
        "precipitation": [timestamp, data.hourly["precipitation"][i]]
      };
      Object.keys(values).forEach(key => {
        result.data[spatialEntity][key].push(values[key]);
      });
    }
    return result;
  }

  async ipmDecisionsRequest(params){
    const self = this;

    const authConfig = {
      "type": "token",
      "request": {
        "config": {
          "method": "post",
          "maxBodyLength": Infinity,
          "url": "https://platform.ipmdecisions.net/api/idp/authorization/authenticate",
          "headers": {
            "client_id": process.env.IPM_DECISIONS_CLIENT_ID,
            "client_secret": process.env.IPM_DECISIONS_CLIENT_SECRET,
            "grant_type": "password",
            "Content-Type": "application/json",
          },
          "data": {
            "email" : process.env.IPM_DECISIONS_EMAIL,
            "password": process.env.IPM_DECISIONS_PASSWORD
          },
        },
      },
    }

    const geometry = utils.transformationFunctions.transformSpatial(params.spatial);
    let configGetDssData = {};

    // first request
    await axios.request("https://platform.ipmdecisions.net/api/dss/rest/pest")
      .then( async (response) => {
        if ( response.data.includes(params.pest) ){
          // second request
          await axios.request("https://platform.ipmdecisions.net/api/dss/rest/crop")
            .then ( async (response) => {
              if ( response.data.includes(params.crop) ){
                // third request
                let locationConfig = {
                  method: "post",
                  maxBodyLength: Infinity,
                  url: "https://platform.ipmdecisions.net/api/dss/rest/dss/location",
                  data: {
                    "type": "FeatureCollection",
                    "features": [{
                      "type": "Feature",
                      "properties": {},
                      "geometry": geometry.geometry
                    }]
                  }
                };
                await axios.request(locationConfig)
                  .then( async (response) => {
                    let dssModelOptions = {};
                    response.data.forEach( (dss) => {
                      dss.models.forEach( (model) => {
                        if ( (model.pests.includes(params.pest)) && (model.crops.includes(params.crop)) ) {
                          dssModelOptions.dssId = dss.id;
                          dssModelOptions.dssName = dss.name;
                          dssModelOptions.dssModelName = model.name,
                          dssModelOptions.dssModelId = model.id,
                          dssModelOptions.dssModelVersion = model.version,
                          dssModelOptions.dssExecutionType = model.execution.type,
                          dssModelOptions.dssEndPoint = dss.id;
                          dssModelOptions.cropEppoCode = params.crop,
                          dssModelOptions.pestEppoCode = params.pest,
                          dssModelOptions.dssVersion = dss.version
                          return;
                        }
                      });
                    })
                    if (Object.keys(dssModelOptions).length == 0) {
                      throw new Error("Location unavailable");
                    }
                    // fourth request - authorization
                    let token = await self.authorizeForService(authConfig);
                    token = "Bearer " + token.data.token;
                    // fifth request - Init farm
                    const initFarmConfig = {
                      method: "post",
                      maxBodyLength: Infinity,
                      url: "https://platform.ipmdecisions.net/api/upr/farms",
                      headers: {
                        "Accept": "application/vnd.h2020ipmdecisions.hateoas+json",
                        "Authorization": token,
                        "Content-Type": "application/json"
                      },
                      data: {
                        "id": null,
                        "name": "FarmCreationCall",
                        "location": {
                          "x": geometry.geometry.coordinates[0],
                          "y": geometry.geometry.coordinates[1],
                          "srid": 4326
                        }
                      }
                    };
                    await axios.request(initFarmConfig)
                      .then( async (response) =>{
                        const farmId = response.data.id;
                        const uriDssBase = `https://platform.ipmdecisions.net/api/upr/farms/${farmId}/dss`;
                        const configInitDss = {
                          method: "post",
                          maxBodyLength: Infinity,
                          url: uriDssBase,
                          headers: {
                            "Accept": "application/json",
                            "Authorization": token,
                            "Content-Type": "application/json",
                          },
			                    data: JSON.stringify([dssModelOptions])
                        };
                        // sixth request - dss init
                        await axios.request(configInitDss)
                          .then( async (response) => {
                            const dssInstanceId = response.data.value[0].id;
                            const uriDssInstance = `https://platform.ipmdecisions.net/api/upr/dss/${dssInstanceId}?days=0`;
                            configGetDssData = {
                              method: "get",
                              maxBodyLength: Infinity,
                              url: uriDssInstance,
                              headers: {
                                "Accept": "application/json",
                                "Accept-Encoding": "gzip,deflate,br",
                                "Authorization": token,
                                "Connection": "keep-alive",
                                "Content-Type": "application/json",
                                "Sec-Fetch-Dest": "empty",
                                "Sec-Fetch-Mode": "cors",
                                "Sec-Fetch-Site": "same-origin",
                                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:124.0) Gecko/20100101 Firefox/124.0",
                                "Referer": "https://platform.ipmdecisions.net/user/dss/dashboard"
                              }
                            };
                          })
                          .catch( (error) => {
                            throw new Error(error);
                          })
                      })
                      .catch( (error) => {
                        throw new Error(error);
                      })
                  })
                  .catch( (error) => {
                    throw new Error(error);
                  })
              } else {
                throw new Error("Crop EPPO Code unavailable");
              }
            })
            .catch( (error) => {
              throw new Error(error);
            })
        } else {
          throw new Error("Pest EPPO Code unavailable");
        }
      })
      .catch( (error) => {
        throw new Error(error);
      })

    return configGetDssData;
  }

  ipmDecisionsResponse(response, params){
    const spatialEntity = JSON.stringify(utils.transformationFunctions.transformSpatial(params.spatial));
    const resultParameters = response.data.resultParameters[0];
    const property = response.data.pestEppoCode;

    let result = {
      "layer": params.layer,
      "operation": "Prediction",
      "data" : {
      }
    };
    result.data[spatialEntity] = {};
    result.data[spatialEntity][property] = []

    resultParameters.data.forEach( (value, index) => {
      const timestamp = utils.dateToTimestamp(resultParameters.labels[index]);
      result.data[spatialEntity][property].push([timestamp, value]);
    });
    return result;
  }

  inputErrorHandling(params, requiredParams) {
    // function to handle input query parameter error handling (missing values)
    requiredParams.forEach( (param) => {
      if ( !params[param] || params[param] === '') {
        throw new Error(`${param} is missing`);
      }
    });
  }

  async handleSurface(params) {
    const self = this;
    const requiredParams = ["property", "spatial", "from", "to"];
    self.inputErrorHandling(params, requiredParams);
    params.layer = "surface";
    params.aggregation = "statistics";
    return await self.fetchDataForSpatial(params);
  }

  async handleAtmosphereCurrentOWM(params) {
    const self = this;
    // input error handling
    const requiredParams = ["spatial"];
    self.inputErrorHandling(params, requiredParams);
    params.layer = "atmosphere";
    params.aggregation = "single";
    params.operation = "current"
    params.provider = "openWeatherMaps"
    return await self.fetchDataForSpatial(params);
  }

  async handleAtmosphereForecastOWM(params) {
    const self = this;
    // input error handling
    const requiredParams = ["spatial"];
    self.inputErrorHandling(params, requiredParams);
    params.layer = "atmosphere";
    params.aggregation = "single";
    params.operation = "forecast"
    params.provider = "openWeatherMaps"
    return await self.fetchDataForSpatial(params);
  }

  async handleAtmosphereForecastOM(params) {
    const self = this;
    // input error handling
    const requiredParams = ["spatial"];
    self.inputErrorHandling(params, requiredParams);
    params.layer = "atmosphere";
    params.aggregation = "single";
    params.operation = "forecast";
    params.provider = "openMeteo";
    return await self.fetchDataForSpatial(params);
  }

  async handleAtmosphereHistoricalOM(params) {
    const self = this;
    // input error handling
    const requiredParams = ["spatial", "from", "to"];
    self.inputErrorHandling(params, requiredParams);
    params.layer = "atmosphere";
    params.aggregation = "single";
    params.operation = "historical";
    params.provider = "openMeteo";
    return await self.fetchDataForSpatial(params);
  }

  async handleBiosphere(params) {
    const self = this;
    const requiredParams = ["property", "spatial", "crop", "pest"];
    self.inputErrorHandling(params, requiredParams);
    params.layer = "biosphere";
    params.aggregation = "single";
    return await self.fetchDataForSpatial(params);
  }

  async fetchDataForSpatial(inRequestParams) {
    const self = this;
    let translatedData;

    if ( inRequestParams.layer == "surface" ){
      const config = await self.sentinelHubRequest(inRequestParams);
      const response = await self.request(config);
      const clusterizedData = self.sentinelHubResponse(response, inRequestParams);
      translatedData = self.interoperabilityTranslator.serializeToJsonLd(clusterizedData);
    }
    if ( inRequestParams.layer == "atmosphere" ) {
      if ( inRequestParams.operation == "current" ) {
        const config = self.openWeatherCurrentRequest(inRequestParams);
        const response = await self.request(config);
        const clusterizedData = self.openWeatherCurrentResponse(response, inRequestParams)
        translatedData = self.interoperabilityTranslator.serializeToJsonLd(clusterizedData);
      } else if ( inRequestParams.operation == "forecast" ) {
        if ( inRequestParams.provider == "openWeatherMaps" ) {
          const config = self.openWeatherForecastRequest(inRequestParams);
          const response = await self.request(config);
          const clusterizedData = self.openWeatherForecastResponse(response, inRequestParams)
          translatedData = self.interoperabilityTranslator.serializeToJsonLd(clusterizedData);
        } else if ( inRequestParams.provider == "openMeteo" ) {
          const config = self.openMeteoForecastRequest(inRequestParams);
          const response = await self.request(config);
          const clusterizedData = self.openMeteoForecastResponse(response, inRequestParams)
          translatedData = self.interoperabilityTranslator.serializeToJsonLd(clusterizedData);
        }
      }
      else if ( inRequestParams.operation == "historical" ) {
        const config = self.openMeteoHistoricalRequest(inRequestParams);
        const response = await self.request(config);
        const clusterizedData = self.openMeteoHistoricalResponse(response, inRequestParams)
        translatedData = self.interoperabilityTranslator.serializeToJsonLd(clusterizedData);
      }
    }
    if ( inRequestParams.layer == "biosphere" ){
      const config = await self.ipmDecisionsRequest(inRequestParams);
      await self.delay(2000);  // experimental for spacing out requests for ipm-decisions API
      const response = await self.request(config);
      const clusterizedData = self.ipmDecisionsResponse(response, inRequestParams);
      translatedData = self.interoperabilityTranslator.serializeToJsonLd(clusterizedData);
    }
    return translatedData;
  }

  async request(config, callback) {
    const self = this;
    try {
      const response = await axios.request(config);
      return response;
    } catch (error) {
      console.log("Error on request:", error);
    }
  }
}

module.exports = DataIntegrator;

const { UserModel } = require('../models');
const bcrypt = require("bcryptjs");
const jwt = require('jwt-simple');
class UserManager {
  constructor(app, secret) {
    this.app = app;
    this.secret = secret;
  }
  async findUser(username) {
    let results = await UserModel.readUsers({ username: username });
    if (results.length === 1) {
      return results[0];
    } else if (results.length > 1) {
      console.error('ERROR', 'More than one user with same username.')
    }
  }
  async loadUser(id) {
    return await UserModel.readUser(id);
  }
  async createUser(username, password, email) {
    const self = this;
    let id = '_u_' + Math.random().toString(36).substr(2, 9);
    let newHashedPass = bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
    let token = jwt.encode({ id: id }, this.secret);
    let params = { id: id, username: username, password: newHashedPass, token: token, email: email, role: 'user' };
    let user = await UserModel.createUser(new UserModel(params));
    delete user.password;
    return user;
  }

  validateLogin(user, password) {
    return bcrypt.compareSync(password, user.password);
  }

  async authenticate(username, password, done) {
    const self = this;
    const user = await this.findUser(username);
    if (!user) {
      return done(null, false, { message: "User does not exist" });
    }
    if (!this.validateLogin(user, password)) {
      return done(null, false, { message: "Password is not valid." });
    }
    delete user.password;
    return done(null, user);
  }
};
module.exports = UserManager;

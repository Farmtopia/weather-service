const express = require('express');
class APIRouter {

  constructor(app, requestor, apiServices) {
    this.app = app;
    this.requestor = requestor;
    this.routes = apiServices;
  }

  initRoutes() {
    const self = this;
    Object.keys(self.routes).forEach((path) => {
      let handlers = self.routes[path];
      self.createRoute(path, handlers);
    });
  }

  createRoute(path, handlers) {
    const self = this;
    const router = new express.Router();
    handlers.forEach((handler) => {
      let fn = async function(request, response, next) {
        try {
          let params = {};
          if (request.method === "GET") {
            params = request.query;
          }
          if (request.method === "POST") {
            params = request.body
          }
          let data = await self.requestor.onHttpRequest(self.requestor.handlers[handler.fn])(request);
          if (data) {
            return response.json(data);
          }
        } catch (err) {
          return next(err);
        }
      }
      router.get('/', fn);
    })
    this.app.use(path, router);
  }

};
module.exports = APIRouter;

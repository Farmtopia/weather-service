const passport = require('passport');
const JWTStrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;
const InteroperabilityTranslator = require('./InteroperabilityTranslator');
class Requestor {

  constructor(app, dataIntegrator, userManager) {
    this.app = app;
    this.di = dataIntegrator;
    this.userManager = userManager;
    this.interoperabilityTranslator = new InteroperabilityTranslator();
    this.passport = passport;
    this.passport.use(new JWTStrategy({
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      jsonWebTokenOptions: {
        ignoreExpiration: false,
      },
      secretOrKey: this.userManager.secret,
      algorithms: ['HS256'],
    }, async (jwtPayload, done) => {
      try {
        const user = await this.userManager.loadUser(jwtPayload.id);
        if (!user) {
          return done(null, false);
        }
        return done(null, user);
      } catch (error) {
        return (error, false);
      }
    }));
  }

  initHandlers() {
    const self = this;
    const dataIntegrator = self.di;
    self.handlers = {
      surface_sentinelhub: dataIntegrator.handleSurface,
      atmosphere_current_openweathermaps: dataIntegrator.handleAtmosphereCurrentOWM,
      atmosphere_forecast_openweathermaps: dataIntegrator.handleAtmosphereForecastOWM,
      atmosphere_forecast_openmeteo: dataIntegrator.handleAtmosphereForecastOM,
      atmosphere_historical_openmeteo: dataIntegrator.handleAtmosphereHistoricalOM,
      biosphere_ipm: dataIntegrator.handleBiosphere,
    }
    return self.handlers;
  }

  onHttpRequest(fn) {
    const self = this;
    return async function (request, user) {
      let params = {};
      if (request.method === "GET") {
        params = request.query;
      }
      if (request.method === "POST") {
        params = request.body
      }
      return await fn.call(self.di, params, user);
    }
  }
};
module.exports = Requestor;

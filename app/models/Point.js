
const mongoose = require('mongoose');
const { APIError } = require('../helpers');
const Schema = mongoose.Schema;
const geoJSONSchema = new Schema({
  type: {
    type: String,
    enum: ['Point', 'Polygon'],
    required: true
  },
  coordinates: {
    type: [], 
    required: true
  }
}, { _id: false, discriminatorKey: 'type' }); 

const spotSchema = new Schema({
  coordinates: [Number] 
});

const polygonSchema = new Schema({
  coordinates: [[[Number]]] 
});
const pointSchema = new Schema({
  id: String,
  title: String,
  type: { type: String, enum: ['station', 'crop', 'field', 'collector', 'route', 'resource', "POI"] }, 
  location: geoJSONSchema,
  root_point_id: String,
  parent_id: String,
  children_ids: [String],
  mc_ids: [String],
},{collection: 'points' });
const GeoJSON = mongoose.model('GeoJSON', geoJSONSchema); 
pointSchema.path('location').discriminator('Point', spotSchema);
pointSchema.path('location').discriminator('Polygon', polygonSchema);
pointSchema.statics = {
  /**
   * Create a Single New Point
   * @param {object} newPoint - an instance of Point
   * @returns {Promise<Point, APIError>}
   */
  async createPoint(newPoint) {
    console.log(newPoint)
    newPoint.id = newPoint.id || '_p_' + Math.random().toString(36).substr(2, 9);
    let point = {};
    point = await newPoint.save();
    return point.toObject();
  },
  /**
   * Delete a single Point
   * @param {String} id - the Point's id
   * @returns {Promise<Point, APIError>}
   */
  async deletePoint(id) {
    const deleted = await this.findOneAndRemove({ id });
    if (!deleted) {
      throw new APIError(404, 'Point Not Found', `No point '${id}' found.`);
    }
    return deleted.toObject();
  },
  /**
   * Get a single Point by id
   * @param {String} id - the Point's id
   * @returns {Promise<Point, APIError>}
   */
  async readPoint(id) {
    const point = await this.findOne({ id });

    if (!point) {
      throw new APIError(404, 'Point Not Found', `No point '${id}' found.`);
    }
    return point.toObject();
  },
  /**
   * Get a list of Points
   * @param {Object} query - pre-formatted query to retrieve points.
   * @param {Object} fields - a list of fields to select or not in object form
   * @param {String} skip - number of docs to skip (for pagination)
   * @param {String} limit - number of docs to limit by (for pagination)
   * @returns {Promise<Points, APIError>}
   */
  async readPoints(query, fields, skip, limit) {
    const points = await this.find(query, fields)
      .skip(skip)
      .limit(limit)
      .sort({ id: 1 })
      .exec();
    if (!points.length) {
      return [];
    }
    return points.map(point => point.toObject());
  },
  /**
   * Patch/Update a single Point
   * @param {String} id - the Point's id
   * @param {Object} pointUpdate - the json containing the Point attributes
   * @returns {Promise<Point, APIError>}
   */
  async updatePoint(id, pointUpdate) {
    const point = await this.findOneAndUpdate({ id }, pointUpdate, {
      new: true
    });
    if (!point) {
      throw new APIError(404, 'Point Not Found', `No point '${id}' found.`);
    }
    return point.toObject();
  }
};
if (!pointSchema.options.toObject) pointSchema.options.toObject = {};
pointSchema.options.toObject.transform = (doc, ret) => {
  const transformed = ret;
  delete transformed._id;
  delete transformed.__v;
  return transformed;
};

/** Ensure MongoDB Indices **/

pointSchema.index({ id: 1, number: 1 }, { unique: true }); 
pointSchema.index({ location: '2dsphere' });

module.exports = mongoose.model('Point', pointSchema);

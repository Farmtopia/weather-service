
const mongoose = require('mongoose');
const { APIError } = require('../helpers');
const Schema = mongoose.Schema;

const predictionSchema = new Schema({
  id: String,
  value: String,
  created_at: { type: Date, default: Date.now },
  timestamp: String,
  source: String,
  spatialEntity: String,
  data_specification: String,
  type: String,
  timeWindow: Number 
},{collection: 'predictions' });

predictionSchema.statics = {

  async createPrediction(newPrediction) {
    newPrediction.id = '_pr_' + Math.random().toString(36).substr(2, 9);
    let prediction = {};
    prediction = await newPrediction.save();
    return prediction.toObject();
  },

  async deletePrediction(id) {
    const deleted = await this.findOneAndRemove({ id });
    if (!deleted) {
      throw new APIError(404, 'Prediction Not Found', `No prediction '${id}' found.`);
    }
    return deleted.toObject();
  },

  async readPrediction(id) {
    const prediction = await this.findOne({ id });

    if (!prediction) {
      throw new APIError(404, 'Prediction Not Found', `No prediction '${id}' found.`);
    }
    return prediction.toObject();
  },

  async readPredictions(query, fields, skip, limit) {
    const predictions = await this.find(query, fields)
      .skip(skip)
      .limit(limit)
      .sort({ id: 1 })
      .exec();
    if (!predictions.length) {
      return [];
    }
    return predictions.map(prediction => prediction.toObject());
  },

  async updatePrediction(id, predictionUpdate) {
    const prediction = await this.findOneAndUpdate({ id }, predictionUpdate, {
      new: true
    });
    if (!prediction) {
      throw new APIError(404, 'Prediction Not Found', `No prediction '${id}' found.`);
    }
    return prediction.toObject();
  }
};
if (!predictionSchema.options.toObject) predictionSchema.options.toObject = {};
predictionSchema.options.toObject.transform = (doc, ret) => {
  const transformed = ret;
  delete transformed._id;
  delete transformed.__v;
  return transformed;
};

predictionSchema.index({ id: 1, number: 1 }, { unique: true }); 

module.exports = mongoose.model('Prediction', predictionSchema);

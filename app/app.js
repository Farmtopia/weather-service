
const dotenv = require('dotenv');
const express = require('express');
const cors = require('cors');
const path = require('path');

const secret = process.env.SECRET;
const UserManager = require('./services/UserManager');

Promise = require('bluebird'); 
const {
  connectToDatabase,
  globalResponseHeaders
} = require('./config');

dotenv.config();
const app = express();

connectToDatabase();
app.use(express.urlencoded({
  extended: true
}));
app.use(express.json({
  type: '*/*'
}));

app.use(cors())

console.log('NODE_ENV', process.env.NODE_ENV);
const {
  DataIntegrator
} = require('./services');

const userManager = new UserManager(app, secret);
app['cloud'] = new DataIntegrator(app, userManager);
app.cloud.init();
app.use(globalResponseHeaders);

app.use('/api',express.static(path.resolve(__dirname, './public'), {
  setHeaders: function(res, filePath) {
    res.contentType(path.basename(filePath));
  }
}));

const session = require("express-session");
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
app.use(session({
   secret: secret
}));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(
  {
     usernameField: "username",
     passwordField: "password"
  },
  (username,password, done) => userManager.authenticate(username, password, done)
));

passport.serializeUser(async (user, done) => {
  
  done(null, user.id);
});
passport.deserializeUser(async (id, done) => {
  const user = await userManager.loadUser(id);
  
  done(null, user);
});
/**
 * This file does NOT run the app. It merely builds and configures it then exports it.config
 *  This is for integration tests with supertest (see __tests__).
 */

module.exports = app;

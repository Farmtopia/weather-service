db.getSiblingDB('admin');
db.createUser({
    user: 'root',
    pwd: 'root',
    roles: [
        {
            role: 'root',
            db: 'admin',
        },
    ],
});
db.getSiblingDB('farmtopiadb');
db.createUser({
    user: 'farmtopia',
    pwd: 'farmtopia',
    roles: [
        {
            role: 'readWrite',
            db: 'farmtopiadb',
        },
    ],
});
db.points.createIndex(
    { "location": "2dsphere" }
  );
FROM node:18

LABEL MAINTAINER Markos Fragkopoulos <ma.fragkopoulos@gmail.com>

RUN npm install pm2@latest --global --quiet
# add local user for security
RUN groupadd -r nodejs \
  && useradd -m -r -g nodejs nodejs

RUN mkdir -p /home/nodejs/app
RUN chown -R nodejs:nodejs /home/nodejs/app
WORKDIR /home/nodejs/app

USER nodejs

# copy local files into container, set working directory and user
COPY . .
COPY --chown=nodejs:nodejs package*.json .

RUN npm install --production --quiet

EXPOSE 5000

CMD ["pm2-runtime", "pm2.json"]

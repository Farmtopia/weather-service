# Data Integration Service

The "Data Integration Service" supports the subscription to external data services and ingestion and integration of these data. There are existing but under-exploited data sources and data products relevant to farming sector that are open for use or available under low-cost subscription schemes.

Currently integrates with: a) weather monitoring and forecast services e.g., OpenWeatherMaps and OpenMeteo API, b) Earth Observation - Analysis Ready Data including from Sentinel-Hub API, c) Pest infestation prediction services from H2020 IPM Decisions.


# Initialization

Edit the docker-compose.yml.example in order to enclose your api tokens. To do so an account should be priorly created on the corresponding vendors (OpenWeatherMaps, Sentinel-Hub, IPM Decisions).

Once edit completed rename the file to docker-compose.yml

Then simple execute in terminal ./runTheStack.sh and you are ready for API requests.


# Create accounts on the corresponding vendors

In order to integrate data from Sentinel-Hub statistical API, a user has to:
1. Sign up at [Sentinel-Hub](https://www.sentinel-hub.com/)
2. Log into Sentinel-Hub and navigate to [Dashboard/User Settings](https://apps.sentinel-hub.com/dashboard/#/account/Settings)
3. Click the "Create" button under "OAuth Clients"
4. Store the client_id and client_secret displayed in docker-compose.yml in the app root directory:
   SENTINEL_HUB_CLIENT_ID: <client_id>
   SENTINEL_HUB_CLIENT_SECRET: <client_secret>

In order to integrate data from OpenWeatherMaps, a user has to:
1. Sign up at [Open Weather Maps](https://home.openweathermap.org/users/sign_up)
2. A Default API Key should automatically be available at https://home.openweathermap.org/api_keys
3. Store the API key in docker-compose.yml in the app’s root directory: 
   OPEN_WEATHER_MAPS_TOKEN: <API Key>

In order to integrate data from IPM Decisions, a user has to:
1. Sign up at [IPM Decisions](https://ipmdecisions.nibio.no/login)
2. Obtain user credentials from IPM Decisions
3. Store client credentials (client_id, client_secret), email and password in docker-compose.yml in the app’s root directory: IPM_DECISIONS_CLIENT_ID: <client_id> IPM_DECISIONS_CLIENT_SECRET: <client_secret>
IPM_DECISIONS_EMAIL: <email> IPM_DECISIONS_PASSWORD: <password>

OpenMeteo is ready for usage, no user action needed.


# Usage

An OpenAPI documentation can be found at http://localhost:5000/api

An example GET request is as follows: http://localhost:5000/api/atmosphere/forecast/openweathermaps?spatial=POINT (35.3032 25.0824)

Requests can be tested using Postman.

The service translates the response in Farmtopia Semantic Model.

* The historical atmosphere service (OpenMeteo) only supports data starting at 2022.

* All services operate on Greenwich Mean Time (GMT)


# Choices and Assumptions

- Spatial input should be in WKT string - no commas - whitespace is important. This is choosed because of the geospatial polygon complexity on the GET request.
- No auth tokens are cached. For services that require authorization tokens (sentinelhub, ipmdecisions) and these tokens are expiring, a new token is requested for each request
- Atmosphere forecast returns the forecast of the next 5 days and Atmosphere rain refers to rain in the last 1 hour
- "@context" in response includes only terms that are present in "@graph"
- OpenMeteo weather data is generated based on the ECMWF forecast model


# Live Endpoint

An instance of the service is up and running for demo purposes. 

Weather Data Forecast (OpenWeatherMaps): http://digi-agri-services.greensupplychain.eu:5000/api/atmosphere/forecast/openweathermaps?spatial=POINT%20(49.006993485744694%202.7970012135169684)

Weather Data Current (OpenWeatherMaps): http://digi-agri-services.greensupplychain.eu:5000/api/atmosphere/current/openweathermaps?spatial=POINT%20(49.006993485744694%202.7970012135169684)

Weather Data Forecast (OpenMeteo): http://digi-agri-services.greensupplychain.eu:5000/api/atmosphere/forecast/openmeteo?spatial=POINT%20(49.006993485744694%202.7970012135169684)

Weather Data Historical (OpenMeteo): http://digi-agri-services.greensupplychain.eu:5000/api/atmosphere/historical/openmeteo?spatial=POINT%20(49.006993485744694%202.7970012135169684))&from=1735693200&to=1738285200

Earth Observation Data: http://digi-agri-services.greensupplychain.eu:5000/api/surface/sentinelhub?property=ndvi&spatial=POLYGON((23.8975089222761%2035.4613346422525,23.8977416723765%2035.4612909823024,23.8978802817428%2035.4613434326858,23.8979795217783%2035.4613632632146,23.8980377994046%2035.4615211140867,23.8980529178218%2035.4617150855363,23.8975683198257%2035.4617558907485,23.8975468469586%2035.4615504986791,23.8975089222761%2035.4613346422525))&from=1688216461&to=1719838861&interval=7

Pest Infestation Data: http://digi-agri-services.greensupplychain.eu:5000/api/biosphere/ipm?property=pest&crop=BRSSS&pest=DASGPA&spatial=POINT%20(49.006993485744694%202.7970012135169684)


API documentation: http://digi-agri-services.greensupplychain.eu:5000/api

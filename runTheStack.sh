#!/bin/bash

# Should be changed
NETWORK_NAME="the-things-stack_default"

# Function to create docker network
create_docker_network() {
  local network_name="$1"

  # Check if the Docker network exists
  if [ -z "$(docker network ls --filter name=${network_name} --quiet)" ]; then
    echo "Creating Docker network: $network_name"
    docker network create $network_name
    echo "Docker network $network_name created."
  else
    echo "Docker network $network_name already exists."
    echo "Moving on..."
  fi
}


echo -e "\e[32mWelcome to farmtopia cloud initializer 🪴🧪🌿\e[0m"
echo ""
if [ -z "$IS_FARMTOPIA_INIT" ]; then
  echo -e "\e[31m🛑✋ Please set IS_FARMTOPIA_INIT in your environment!\e[0m"
  echo ""
  echo "To do so please copy and paste the following one liner:"
  echo ""
  echo "echo 'export IS_FARMTOPIA_INIT=false' >> ~/.bashrc"
  echo "source ~/.bashrc"
elif [ "$IS_FARMTOPIA_INIT" = "true" ]; then
  echo "Let's go."
else
  echo "Let me set up some stuff for you.."
  create_docker_network $NETWORK_NAME
  echo "Let's try to build the stack with docker"
  docker compose up -d
  echo ""
  echo ""
  echo "Let's hope the application managed to connect to the db with this little trick and hopefully you are set!🚀"
  docker compose restart 
  echo ""
  echo "CU!"
fi